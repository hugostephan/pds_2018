package projet_web_univers7.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import projet_web_univers7.domain.CarModel;
import projet_web_univers7.services.CarModelService;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class CarModelControllerTest {

    private MockMvc mockMvc;

    @Mock
    private CarModelService carModelService;

    @InjectMocks
    private CarModelController carModelController;

    @Before
    public void setup() {

        // Process mock annotations
        MockitoAnnotations.initMocks(this);

        // Setup Spring test in standalone mode
        this.mockMvc = MockMvcBuilders.standaloneSetup(carModelController).build();

    }

    @Test
    public void searchReturnRightList() throws Exception {
        CarModel cm1 = new CarModel("Peugeot","308","5",new BigDecimal(5),"pic1","Citadine",5);
        CarModel cm2 = new CarModel("Citroen","C1","6",new BigDecimal(5),"pic2","Citadine",5);

        when(carModelService.findAllModel()).thenReturn(Arrays.asList(cm1,cm2));

        this.mockMvc.perform(post("/searchReferentielTarifCM")
                .param("brand", "Peugeot")
                .param("model", "308")
                .param("gas_consumption", "")
                .param("hourPriceBegin", "")
                .param("hourPriceEnd", ""))
            .andExpect(status().isOk())
            .andExpect(view().name("priceRefListCarModel"))
            .andExpect(model().attribute("listCarModel", hasSize(1)))
            .andExpect(model().attribute("listCarModel", hasItem(
                    allOf(
                            hasProperty("brand",is("Peugeot")),
                            hasProperty("model",is("308")),
                            hasProperty("gas_consumption",is("5")),
                            hasProperty("hour_price",is(new BigDecimal(5))),
                            hasProperty("picture",is("pic1")),
                            hasProperty("type", is("Citadine")),
                            hasProperty("capacity", is(5))
                    )
            )));
    }
}
