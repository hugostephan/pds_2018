package projet_web_univers7.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import projet_web_univers7.domain.*;
import projet_web_univers7.repositories.HistoricRepository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * DelayService Uniy Test Class with mockito
 */
@RunWith(MockitoJUnitRunner.class)
public class DelayServiceImplTest {
    // Config
    @Mock
    HistoricRepository historicRepository;
    @InjectMocks
    DelayService delayService = new DelayServiceImpl();

    ArrayList<Historic> historicList;
    Historic historic;
    Account acc;
    Role role;
    CarModel carModel;
    Car car;
    Place place;


    /**
     * Initialize mockito and test data
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        this.initData();
        this.initMockito();
    }

    /**
     * Initialize test data
     * @throws Exception
     */
    private void initData() throws Exception {

        // var init
        acc = new Account();
        acc.setAddress("Test Address");
        acc.setBirthDate("08/05/1994");
        acc.setCity("Creteil");
        acc.setEmail("hugostephanbac@gmail.com");
        acc.setFirstname("Hugo");
        acc.setLastname("STEPHAN");
        acc.setPassword("azertyuiop");
        acc.setPasswordConfirm("azertyuiop");
        acc.setId(1);
        acc.setPhoneNumber("0123456789");
        acc.setPostalCode("94380");
        acc.setUsername("hugo.stephan");

        role = new Role();
        role.setCode("BO");
        role.setDescription("Back Office User : can view and edit data.");
        role.setId(1);
        role.setName("backoffice");

        acc.setRole(role);
        carModel = new CarModel(1,"208","Peugeot","Essence",new BigDecimal(17),"Test","Citadine",5);
        car = new Car(1,carModel,2017,"BP228TA");
        place=new Place();
        place.setWidth("150");
        place.setLength("150");
        place.setName("a1");
        place.setId(1);

        int i;

        historicList = new ArrayList<>();
        for(i=0;i<200;i++)
        {
            historic = new Historic();
            historic.setAccount(acc);
            historic.setCar(car);
            historic.setEndPlace(place);
            historic.setStartPlace(place);
            if(i%15 == 0)
            {
                historic.setState("OVER_WITH_DELAY");
            }
            else
            {
                historic.setState("OVER_NORMAL");
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
            Date parsedDateEnd = dateFormat.parse(new String ("0"+(i+1)%11+"/0"+(i+1)%11+"/2018 " + i%5 + ":30"));
            Timestamp timestampStart = new java.sql.Timestamp(parsedDateEnd.getTime());
            historic.setPrevisionEndTimestamp(timestampStart);
            if(i%15 == 0)
            {
                Date parsedDate = dateFormat.parse(new String ("0"+(i+1)%11+"/0"+(i+1)%11+"/2018 " + i%5 + ":55"));
                Timestamp timestampEnd = new java.sql.Timestamp(parsedDate.getTime());
                historic.setRealEndTimestamp(timestampEnd);
            }
            else
            {
                Date parsedDate = dateFormat.parse(new String ("0"+(i+1)%11+"/0"+(i+1)%11+"/2018 " + i%5 + ":10"));
                Timestamp timestampEnd = new java.sql.Timestamp(parsedDate.getTime());
                historic.setRealEndTimestamp(timestampEnd);
            }

            Date parsedDatePrevisionStart = dateFormat.parse(new String ("0"+(i+1)%11+"/0"+(i+1)%11+"/2018 " + i%5 + ":00"));
            Timestamp timestampPrevisionStart = new java.sql.Timestamp(parsedDatePrevisionStart.getTime());
            historic.setPrevisionStartTimestamp(timestampPrevisionStart);

            Date parsedDateRealStart = dateFormat.parse(new String ("0"+(i+1)%11+"/0"+(i+1)%11+"/2018 " + i%5 + ":00"));
            Timestamp timestampRealStart = new java.sql.Timestamp(parsedDateRealStart.getTime());
            historic.setRealStartTimestamp(timestampRealStart);

            historicList.add(historic);
        }
    }

    /**
     * Initialize Mockito annotations and stub historic repository calls
     * @throws Exception
     */
    private void initMockito() throws Exception {
        // init annotations
        MockitoAnnotations.initMocks(this);

        // stubbing
        Mockito.when(this.historicRepository.findAll()).thenReturn(historicList);
        Mockito.when(this.historicRepository.getNumberHistoric()).thenReturn(200);
    }

    /**
     * Test getNumberDelay function
     * @throws Exception
     */
    @Test
    public void getNumberDelay() throws Exception {
        // init
        int nb = 14 ;

        // test
        assertEquals(nb, delayService.getNumberDelay());

        // verify
        Mockito.verify(this.historicRepository, Mockito.times(1)).findAll();
    }

    /**
     * Test getRateDelay function
     * @throws Exception
     */
    @Test
    public void getRateDelay() throws Exception {
        // init
        double rate = 0.07 ;

        // test
        assertEquals(rate, delayService.getRateDelay(), 0.00000);

        // verify
        Mockito.verify(this.historicRepository, Mockito.times(1)).getNumberHistoric();
    }

    /**
     * Test getNumberHistoric function
     * @throws Exception
     */
    @Test
    public void getNumberHistoric() throws Exception {
        // init
        int nb = 200 ;

        // test
        assertEquals(nb, delayService.getNumberHistoric());

        // verify
        Mockito.verify(this.historicRepository, Mockito.times(1)).getNumberHistoric();
    }

    /**
     * Test getNumberDelayByMonth function
     * @throws Exception
     */
    @Test
    public void getNumberDelayByMonth() throws Exception {
        // init
        int nb_fevrier = 1 ;
        int nb_janvier = 2 ;

        // test
        assertEquals(nb_janvier, delayService.getNumberDelayByMonth("01"));
        assertEquals(nb_fevrier, delayService.getNumberDelayByMonth("02"));

        // verify
        Mockito.verify(this.historicRepository, Mockito.times(2)).findAll();
    }

    /**
     * Test getRateDelayByMonth function
     * @throws Exception
     */
    @Test
    public void getRateDelayByMonth() throws Exception {
        // init
        double rate_janvier = 0.1053 ;
        double rate_fevrier = 0.0526 ;

        // test
        assertEquals(rate_janvier, delayService.getRateDelayByMonth("01"), 0.0001);
        assertEquals(rate_fevrier, delayService.getRateDelayByMonth("02"), 0.0001);

        // verify
        Mockito.verify(this.historicRepository, Mockito.times(4)).findAll();
    }

    /**
     * Test getNumberHistoricByMonth function
     * @throws Exception
     */
    @Test
    public void getNumberHistoricByMonth() throws Exception {
        // init
        int nb = 19;

        // test
        assertEquals(nb, delayService.getNumberHistoricByMonth("01"));
        assertEquals(nb, delayService.getNumberHistoricByMonth("02"));

        // verify
        Mockito.verify(this.historicRepository, Mockito.times(2)).findAll();
    }
}