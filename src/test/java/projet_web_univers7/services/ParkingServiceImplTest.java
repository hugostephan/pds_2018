package projet_web_univers7.services;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;
import projet_web_univers7.repositories.ParkingRepository;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class ParkingServiceImplTest {
    @Mock
    ParkingRepository parkingRepository;

    @InjectMocks
    ParkingServiceImpl parkingService = new ParkingServiceImpl();

    private Parking parking;
    private Place place;
    private List<Place> placeList;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        parking = new Parking();
        parking.setId(new Long(1));
        parking.setName("toto");
        parking.setLongitude("102.51");
        parking.setLatitude("154.12");
        parking.setAddress("12 rue toto");
        parking.setCapacity(3);

        place=new Place();
        place.setWidth("150");
        place.setLength("150");
        place.setName("a1");
        place.setId(1);

        placeList=new ArrayList<Place>();
        placeList.add(place);

        parking.setPlaceCollection(placeList);
    }

    @Test
    public void listAllParking() throws Exception{
        parkingService.listAllParking();

        Mockito.verify(this.parkingRepository, times(1)).findAll();
    }

    @Test
    public void getParkingById() throws Exception{
        Mockito.when(parkingRepository.findById(parking.getId())).thenReturn(parking);

        parkingService.getParkingById(parking.getId().intValue());
        Mockito.verify(this.parkingRepository, times(1)).findById(parking.getId());
    }

    @Test
    public void saveParking() throws Exception{
        Mockito.when(parkingRepository.save(parking)).thenReturn(parking);
        parkingService.saveParking(parking);
        Mockito.verify(this.parkingRepository,times(1)).save(parking);
    }

}
