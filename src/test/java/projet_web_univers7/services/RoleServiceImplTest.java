package projet_web_univers7.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import projet_web_univers7.domain.Role;
import projet_web_univers7.repositories.RoleRepository;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceImplTest {
    @Mock
    RoleRepository roleRepository;
    @InjectMocks
    RoleServiceImpl roleService = new RoleServiceImpl();

    // Beans
    Role role ;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        role = new Role();
        role.setCode("BO");
        role.setDescription("Back Office User : can view and edit data.");
        role.setId(1);
        role.setName("backoffice_user");
    }

    @Test
    public void listAllRole() throws Exception {
        // stubbing
        ArrayList<Role> l = new ArrayList<Role>();
        l.add(role);
        Mockito.when(this.roleRepository.findAll()).thenReturn(l);

        // test
        roleService.listAllRole();

        // verify
        Mockito.verify(roleRepository, times(1)).findAll();
    }

    @Test
    public void getRoleById() throws Exception {
        // stubbing
        Mockito.when(this.roleRepository.findOne(role.getId())).thenReturn(role);

        // test
        roleService.getRoleById(role.getId());

        // verify
        Mockito.verify(this.roleRepository, times(1)).findOne(role.getId());
        assertEquals(role, roleService.getRoleById(role.getId()));
    }

    @Test
    public void saveRole() throws Exception {
        // stubbing
        Mockito.when(this.roleRepository.save(role)).thenReturn(role);

        // test
        roleService.saveRole(role);

        // verify
        Mockito.verify(this.roleRepository, times(1)).save(role);
    }

    @Test
    public void deleteRole() throws Exception {
        // test
        roleService.deleteRole(role.getId());

        // verify
        Mockito.verify(this.roleRepository, times(1)).delete(role.getId());
    }

}