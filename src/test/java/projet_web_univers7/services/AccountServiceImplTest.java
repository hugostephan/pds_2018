package projet_web_univers7.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import projet_web_univers7.domain.Account;
import projet_web_univers7.domain.Role;
import projet_web_univers7.repositories.AccountRepository;
import projet_web_univers7.repositories.RoleRepository;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {
    // Mockito Config
    @Mock
    AccountRepository accountRepository ;
    @Mock
    RoleRepository roleRepository;
    @Mock
    BCryptPasswordEncoder bCryptPasswordEncoder;
    @InjectMocks
    AccountServiceImpl accountService = new AccountServiceImpl();


    // Beans
    private ArrayList<Account> accounts;
    private Account acc ;
    private Role role ;


    /**
     * Set up mokito annotations and create a Club useful for testing
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        this.initData();
        this.initMockito();
    }

    private void initMockito() throws Exception {
        // init annotations
        MockitoAnnotations.initMocks(this);

        // stubbing
        Mockito.when(this.accountRepository.save(acc)).thenReturn(acc);
        Mockito.when(this.bCryptPasswordEncoder.encode(acc.getPassword())).thenReturn(acc.getPassword());
        Mockito.when(this.bCryptPasswordEncoder.encode(acc.getPasswordConfirm())).thenReturn(acc.getPasswordConfirm());
        Mockito.when(this.roleRepository.findOne(1)).thenReturn(role);
        Mockito.when(this.accountRepository.findByUsername(acc.getUsername())).thenReturn(acc);
        Mockito.when(this.accountRepository.findById(acc.getId())).thenReturn(acc);
        Mockito.when(this.accountRepository.findAll()).thenReturn(accounts);
    }

    private void initData() throws Exception {

        // var init
        acc = new Account();
        acc.setAddress("Test Address");
        acc.setBirthDate("08/05/1994");
        acc.setCity("Creteil");
        acc.setEmail("hugostephanbac@gmail.com");
        acc.setFirstname("Hugo");
        acc.setLastname("STEPHAN");
        acc.setPassword("azertyuiop");
        acc.setPasswordConfirm("azertyuiop");
        acc.setId(1);
        acc.setPhoneNumber("0123456789");
        acc.setPostalCode("94380");
        acc.setUsername("hugo.stephan");

        role = new Role();
        role.setCode("BO");
        role.setDescription("Back Office User : can view and edit data.");
        role.setId(1);
        role.setName("backoffice");

        acc.setRole(role);
        accounts = new ArrayList<Account>();
        accounts.add(acc);
    }

    @Test
    public void saveAccountNoUsername() throws Exception
    {
        // setting no username to account
        acc.setUsername(null);

        // test
        assertEquals(null, accountService.saveAccount(acc));

        // verify
        Mockito.verify(this.accountRepository, times(0)).save(acc);
        Mockito.verify(this.bCryptPasswordEncoder, times(0)).encode(acc.getPassword());
    }

    @Test
    public void saveAccountRegistration() throws Exception {
        acc.setUsername("different_username");
        // test
        assertNotEquals(null, accountService.saveAccount(acc));

        // verify
        Mockito.verify(this.accountRepository, times(1)).save(acc);
        Mockito.verify(this.bCryptPasswordEncoder, times(2)).encode(acc.getPassword());
    }

    @Test
    public void saveAccountNoPasswordExistingAccount() throws Exception
    {
        // setting no password to existing account
        acc.setPassword(null);

        // test
        assertEquals(acc.getId(), accountService.saveAccount(acc).getId());

        // verify
        Mockito.verify(this.accountRepository, times(1)).save(acc);
        Mockito.verify(this.bCryptPasswordEncoder, times(0)).encode(acc.getPassword());
    }

    @Test
    public void saveAccountNoPassword() throws Exception
    {
        // Creating new empty account (so with no password)
        Account a = new Account();

        // Test
        assertEquals(null, accountService.saveAccount(a));

        // verify
        Mockito.verify(this.accountRepository, times(0)).save(acc);
        Mockito.verify(this.bCryptPasswordEncoder, times(0)).encode(acc.getPassword());
    }

    @Test
    public void saveAccountAutoRegDate() throws Exception
    {
        String pattern = "dd/MM/yyyy";
        String regDate =new SimpleDateFormat(pattern).format(new Date());

        // Creating new empty account (so with no password)
        Account a = acc;
        a.setUsername("diff_username");

        // Test
        assertEquals(a, accountService.saveAccount(a));
        assertEquals(regDate, a.getRegistrationDate());

        // verify
        Mockito.verify(this.accountRepository, times(1)).save(acc);
        Mockito.verify(this.bCryptPasswordEncoder, times(2)).encode(acc.getPassword());
    }


    @Test
    public void findByUsername() throws Exception {
        // testing
        accountService.findByUsername(acc.getUsername());

        // verify
        Mockito.verify(this.accountRepository, times(1)).findByUsername(acc.getUsername());
    }

    @Test
    public void findByIdBadId() throws Exception {
        // testing bad id call return null
        assertEquals(null,accountService.findById("bullshit"));

        // verify
        Mockito.verify(this.accountRepository, times(0)).findById(acc.getId());
    }

    @Test
    public void findById() throws Exception {
        // testing
        assertEquals(acc,accountService.findById(""+acc.getId()));

        // verify
        Mockito.verify(this.accountRepository, times(1)).findById(acc.getId());
    }

    @Test
    public void findAllByRole() throws Exception {
        // testing
        assertEquals(accounts,accountService.findAllByRole(acc.getRole().getName()));

        // verify
        Mockito.verify(this.accountRepository, times(1)).findAll();
    }

    @Test
    public void findAllByRoleBadRole() throws Exception {
        ArrayList <Account> no_account = new ArrayList<> ();

        // testing no account are returned when looking for non existing role
        assertEquals(no_account,accountService.findAllByRole("bullshit"));
    }


}