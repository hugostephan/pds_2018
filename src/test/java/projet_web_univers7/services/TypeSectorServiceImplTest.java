package projet_web_univers7.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import projet_web_univers7.domain.TypeSector;
import projet_web_univers7.repositories.TypeSectorRepository;
import projet_web_univers7.services.TypeSectorService;
import projet_web_univers7.services.TypeSectorServiceImpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TypeSectorServiceImplTest {

    @Mock
    TypeSectorRepository typeSectorRepo;

    @InjectMocks
    TypeSectorServiceImpl tsservice = new TypeSectorServiceImpl();

    private TypeSector ts;


    @Before
    public void init() throws Exception{
        MockitoAnnotations.initMocks(this);

        ts = new TypeSector();
        ts.setColor("green");
        ts.setId(1);
        ts.setPercent(0.2);
    }
    @Test
    public void testUpdateSector() throws Exception {


        Mockito.when(this.typeSectorRepo.save(ts)).thenReturn(ts);

        tsservice.saveTypeSector(ts);
        verify(typeSectorRepo,times(1)).save(any(TypeSector.class));

        ts.setPercent(0.12);
        tsservice.saveTypeSector(ts);

        assertEquals(0.12,ts.getPercent(),0);
        verify(typeSectorRepo,times(2)).save(any(TypeSector.class));
    }
}

