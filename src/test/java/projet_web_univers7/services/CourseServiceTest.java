package projet_web_univers7.services;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import projet_web_univers7.domain.Account;
import projet_web_univers7.domain.Course;
import projet_web_univers7.domain.Place;
import projet_web_univers7.domain.Reservation;
import projet_web_univers7.repositories.CourseRepository;

public class CourseServiceTest {

    @Mock
    private CourseRepository courseRepo;

    @InjectMocks
    private CourseService courseService= new CourseService();

    List<Course> lc;
    Timestamp d1;
    Account acc;
    Place place;
    Course c1,c2,c3;

    @Before
    public void setUp() throws Exception {
        // init annotations
        MockitoAnnotations.initMocks(this);

        // init var
        lc= new ArrayList<Course>();
        d1 = new Timestamp(System.currentTimeMillis());
        Timestamp d2 = new Timestamp(System.currentTimeMillis()+3600*1000);
        Timestamp d3 = new Timestamp(System.currentTimeMillis()-3600*1000);
        Timestamp d4 = new Timestamp(System.currentTimeMillis()+60*1000);

        acc = new Account();
        acc.setAddress("Test Address");
        acc.setBirthDate("23/06/1994");
        acc.setCity("Madrid");
        acc.setEmail("zizou@gmail.com");
        acc.setFirstname("Zinedine");
        acc.setLastname("ZIDANE");
        acc.setPassword("azertyuiop");
        acc.setPasswordConfirm("azertyuiop");
        acc.setId(1);
        acc.setPhoneNumber("0123456789");
        acc.setPostalCode("94000");
        acc.setUsername("zinedine.zidane");

        place=new Place();
        place.setWidth("150");
        place.setLength("150");
        place.setName("a1");
        place.setId(1);

        Reservation r1 = new Reservation(d1,d2,place,acc);
        Reservation r2 = new Reservation(d1,d2,place,acc);
        Reservation r3 = new Reservation(d3,d4,place,acc);

        c1 = new Course(r1,d1,d2,"2.45","48.9333");
        c2 = new Course(r2,d1,d2,"2.4667","48.7833");
        c3 = new Course(r3,d3,d4,"2.3599","48.9354");

        lc.add(c1);
        lc.add(c2);
        lc.add(c3);

        //stubbing
        Mockito.when(this.courseRepo.findAllCourses(any(Timestamp.class))).thenReturn(lc);
        Mockito.when(this.courseRepo.save(c1)).thenReturn(c1);

    }

    @Test
    public void getAllCourseTest(){
        // test size
        assertEquals(3,courseService.getAllCourse().size());

        //test some random parameters
        assertEquals("2.45",courseService.getAllCourse().get(0).getLongitude());
        assertEquals("48.7833",courseService.getAllCourse().get(1).getLatitude());
        assertEquals("2.3599",courseService.getAllCourse().get(2).getLongitude());


        Mockito.verify(courseRepo,Mockito.times(4)).findAllCourses(any(Timestamp.class));
    }

    @Test
    public void insertCourseTest(){

        //test
        assertEquals(c1,courseService.insertCourse(c1));

        //verify
        Mockito.verify(courseRepo,Mockito.times(1)).save(c1);
    }

    @Test
    public void updateRealEndTimestampTest(){
        courseService.updateRealEndTimestamp(d1,1);

        Mockito.verify(courseRepo,Mockito.times(1)).setRealEndTimestamp(d1,1);
    }
}
