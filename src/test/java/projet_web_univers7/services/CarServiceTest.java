package projet_web_univers7.services;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import projet_web_univers7.domain.Car;
import projet_web_univers7.domain.CarModel;

import projet_web_univers7.repositories.CarRepo;
import projet_web_univers7.services.CarService;

public class CarServiceTest {

	@Mock
	private CarRepo carRepository ;
	@InjectMocks
	private CarService carService = new CarService();	
	
	@Before
	public void setUp() throws Exception {
	    MockitoAnnotations.initMocks(this);
	}
	@Test
	public void saveVehicleTestOk() throws Exception {
		CarModel m = new CarModel(1,"208","Peugeot","Essence",new BigDecimal(17),"Test","Citadine",5);
		List<Car> list = new ArrayList<Car>();
		Car v = new Car(1,m,2017,"BP228TA");
		Car c = new Car(1,m,2017,"BP238TA");
		list.add(c);
		
		// stubbing 
		Mockito.when(carRepository.save(v)).thenReturn(v);
		Mockito.when(carRepository.findAll()).thenReturn(list);
		// test
		Car vehicle = carService.insertCar(v);
		// verification 
		Mockito.verify(carRepository).save(v);
	}
	
	@Test
	public void saveVehiculeTestFail() throws Exception{
		CarModel m = new CarModel(1,"208","Peugeot","Essence",new BigDecimal(17),"Test","Citadine",5);
		List<Car> list = new ArrayList<Car>();
		Car v = new Car(1,m,2017,"BP228TA");
		Car c = new Car(1,m,2017,"BP228TA");
		list.add(c);
		
		// stubbing 
		
		Mockito.when(carRepository.findAll()).thenReturn(list);
		// test
		Car vehicle = carService.insertCar(v);
		// verification 
		Mockito.verify(carRepository, Mockito.times(0)).save(v);
		
	}
	
	@Test
	public void getAllCarsTest() throws Exception{
		List<Car> l = new ArrayList<Car>();
		
		// stubbing 
		Mockito.when(carRepository.findAll()).thenReturn(l);
		
		//Test
		List<Car> list = carService.getAllCars();
		
		// verification 
		Mockito.verify(carRepository).findAll();
	}
	
	@Test
	public void getDistinctLn() {
		String ln= "BP228TA";
		String ln2 = "CJ509ME";
		List<String> l = new ArrayList<String>();
		List<String> local = new ArrayList<String>();
		
		local.add(ln);
		local.add(ln2);
		l.add(ln);
		l.add(ln2);
		
		// stubbing 
		Mockito.when(carRepository.getByLn()).thenReturn(l);
		
		//Test
		List<String> list = carService.getDistinctLn();
		
		// verification 
		Mockito.verify(carRepository).getByLn();
		assertEquals(local, l);
	}
	
	@Test
	public void getDistinctYear() {
		String year= "2018";
		String yeah = "2017";
		List<String> l = new ArrayList<String>();
		List<String> local = new ArrayList<String>();
		
		local.add(year);
		local.add(yeah);
		l.add(year);
		l.add(yeah);
		
		// stubbing 
		Mockito.when(carRepository.getByYear()).thenReturn(l);
		
		//Test
		List<String> list = carService.getDistinctYear();
		
		// verification 
		Mockito.verify(carRepository).getByYear();
		assertEquals(local, l);
	}
	

}
