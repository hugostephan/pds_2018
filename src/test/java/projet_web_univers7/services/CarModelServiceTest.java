package projet_web_univers7.services;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import projet_web_univers7.domain.Car;
import projet_web_univers7.domain.CarModel;
import projet_web_univers7.repositories.CarModelRepo;
import projet_web_univers7.repositories.CarRepo;
import projet_web_univers7.services.CarModelService;
import projet_web_univers7.services.CarService;

public class CarModelServiceTest {


	@Mock
	private CarModelRepo carModelRepository ;
	@InjectMocks
	private CarModelService carModelService = new CarModelService();	
	
	@Before
	public void setUp() throws Exception {
	    MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void saveModel() throws Exception {
		CarModel m = new CarModel(1,"208","Peugeot","Essence",new BigDecimal(17),"Test","Citadine",5);
		
		// stubbing 
		Mockito.when(carModelRepository.save(m)).thenReturn(m);
		
		// test
		CarModel cm = carModelService.insertModel(m);
		// verification 
		Mockito.verify(carModelRepository).save(m);
	}
	
	@Test
	public void getAllModels() {
		List<String> l = new ArrayList<String>();
		
		// stubbing 
		Mockito.when(carModelRepository.getModelName()).thenReturn(l);
		
		//Test
		List<String> list = carModelService.getAllModels();
		
		// verification 
		Mockito.verify(carModelRepository).getModelName();
	}
	
	@Test
	public void getDistinctBrand() {
		String peugeot= "Peugeot";
		String renault = "Renault";
		List<String> l = new ArrayList<String>();
		List<String> local = new ArrayList<String>();
		
		local.add(peugeot);
		local.add(renault);
		l.add(peugeot);
		l.add(renault);
		// stubbing 
		Mockito.when(carModelRepository.getByBrand()).thenReturn(l);
		
		//Test
		List<String> list = carModelService.getDistinctBrand();
		
		// verification 
		Mockito.verify(carModelRepository).getByBrand();
		assertEquals(local, l);
	}
	
	@Test
	public void getDistinctModel() {
		String model= "3008";
		String clio = "clio";
		List<String> l = new ArrayList<String>();
		List<String> local = new ArrayList<String>();
		
		local.add(model);
		local.add(clio);
		l.add(model);
		l.add(clio);
		// stubbing 
		Mockito.when(carModelRepository.getByModel()).thenReturn(l);
		
		//Test
		List<String> list = carModelService.getDistinctModel();
		
		// verification 
		Mockito.verify(carModelRepository).getByModel();
		assertEquals(local, l);
	}
	
	@Test
	public void getDistinctType() {
		String citadine= "citadine";
		String espace = "espace";
		List<String> l = new ArrayList<String>();
		List<String> local = new ArrayList<String>();
		
		local.add(citadine);
		local.add(espace);
		l.add(citadine);
		l.add(espace);
		// stubbing 
		Mockito.when(carModelRepository.getByType()).thenReturn(l);
		
		//Test
		List<String> list = carModelService.getDistinctType();
		
		// verification 
		Mockito.verify(carModelRepository).getByType();
		assertEquals(local, l);
	}
	
	@Test
	public void getDistinctCapacity() {
		String cinq= "cinq";
		String trois = "trois";
		List<String> l = new ArrayList<String>();
		List<String> local = new ArrayList<String>();
		
		local.add(cinq);
		local.add(trois);
		l.add(cinq);
		l.add(trois);
		
		// stubbing 
		Mockito.when(carModelRepository.getByCapacity()).thenReturn(l);
		
		//Test
		List<String> list = carModelService.getDistinctCapacity();
		
		// verification 
		Mockito.verify(carModelRepository).getByCapacity();
		assertEquals(local, l);
	}
	
	@Test
	public void getDistinctGs() {
		String essence= "essence";
		String diesel = "diesel";
		List<String> l = new ArrayList<String>();
		List<String> local = new ArrayList<String>();
		
		local.add(essence);
		local.add(diesel);
		l.add(essence);
		l.add(diesel);
		
		// stubbing 
		Mockito.when(carModelRepository.getByGs()).thenReturn(l);
		
		//Test
		List<String> list = carModelService.getDistinctGs();
		
		// verification 
		Mockito.verify(carModelRepository).getByGs();
		assertEquals(local, l);
	}
	
	@Test
	public void findModelByBrand() {
		String brand = "Peugeot";
		String model = "207";
		CarModel m = new CarModel(1,"208","Peugeot","Essence",new BigDecimal(17),"Test","Citadine",5);
		
		// stubbing 
		Mockito.when(carModelRepository.getCarModelByBrandModel(brand,model)).thenReturn(m);
		
		//Test
		CarModel mo = carModelService.findCarModelByBrand(brand, model);
		
		// verification 
		Mockito.verify(carModelRepository).getCarModelByBrandModel(brand,model);
	}
}
