package projet_web_univers7.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;
import projet_web_univers7.repositories.ParkingRepository;
import projet_web_univers7.repositories.PlaceRepository;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class PlaceServiceImplTest {

    @Mock
    PlaceRepository placeRepository;

    @Mock
    ParkingRepository parkingRepository;

    @InjectMocks
    PlaceServiceImpl placeService=new PlaceServiceImpl();

    private Parking parking;
    private Place place;
    private List<Place> placeList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        place=new Place();
        place.setWidth("150");
        place.setLength("150");
        place.setName("a1");
        place.setId(1);

        parking = new Parking();
        parking.setId(new Long(1));
        parking.setName("toto");
        parking.setLongitude("102.51");
        parking.setLatitude("154.12");
        parking.setAddress("12 rue toto");
        parking.setCapacity(3);

        place.setParking(parking);

        placeList = new ArrayList<Place>();
        placeList.add(place);

        parking.setPlaceCollection(placeList);

    }

    @Test
    public void addPlace() throws Exception{
        Mockito.when(placeRepository.save(place)).thenReturn(place);
        placeService.addPlace(place, parking.getId().intValue());

        Mockito.verify(this.parkingRepository, times(1)).findById(parking.getId());
        Mockito.verify(this.placeRepository, times(1)).save(place);
    }

    @Test
    public void listPlaceByParking() throws Exception{
        Mockito.when(placeRepository.findAllByParking(parking)).thenReturn(placeList);
        placeService.listPlaceByParking(parking);
        Mockito.verify(this.parkingRepository, times(1)).findById(parking.getId());
        Mockito.verify(this.placeRepository,times(1)).findAllByParking(parkingRepository.findById(parking.getId()));
    }

    @Test
    public void listAll() throws Exception{
        Mockito.when(placeRepository.findAll()).thenReturn(placeList);
        placeService.listAll();
        Mockito.verify(this.placeRepository,times(1)).findAll();
    }

}
