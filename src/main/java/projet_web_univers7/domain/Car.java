package projet_web_univers7.domain;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="car")
public class Car {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_car;
	@ManyToOne
	@JoinColumn(name = "id_car_model")
	private CarModel car_model=null;
	private int created_year;
	private String licence_number;
	
	public Car() {
	
	}

	public Car(int id_car, CarModel car_model, int created_year, String licence_number) {
		super();
		this.id_car = id_car;
		this.car_model = car_model;
		this.created_year = created_year;
		this.licence_number = licence_number;
	}
	
	public Car(CarModel car_model, int created_year, String licence_number) {
		this.id_car = id_car;
		this.car_model = car_model;
		this.created_year = created_year;
		this.licence_number = licence_number;
	}

	public int getId_car() {
		return id_car;
	}

	public void setId_car(int id_car) {
		this.id_car = id_car;
	}

	public CarModel getCar_model() {
		
		return car_model;
	}

	public void setCar_model(CarModel car_model) {
		this.car_model = car_model;
	}

	public int getCreated_year() {
		return created_year;
	}

	public void setCreated_year(int created_year) {
		this.created_year = created_year;
	}

	public String getLicence_number() {
		return licence_number;
	}

	public void setLicence_number(String licence_number) {
		this.licence_number = licence_number;
	}

	@Override
	public String toString() {
		return "Car [id_car=" + id_car + ", car_model=" + car_model + ", created_year=" + created_year
				+ ", licence_number=" + licence_number + "]";
	}

	
	
	
}
