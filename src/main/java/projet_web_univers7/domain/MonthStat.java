package projet_web_univers7.domain;

public class MonthStat {
    private String month;
    private String rate;
    private int nb_delay;
    private int nb_histo;

    public String getMonth() {
        return month;
    }

    public void setMonth(String motnh) {
        this.month = motnh;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public int getNb_delay() {
        return nb_delay;
    }

    public void setNb_delay(int nb_delay) {
        this.nb_delay = nb_delay;
    }

    public int getNb_histo() {
        return nb_histo;
    }

    public void setNb_histo(int nb_histo) {
        this.nb_histo = nb_histo;
    }
}
