package projet_web_univers7.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.time.Period;
import java.util.Date;
import java.util.List;

@Entity
public class Bill {

    @Id
    @GeneratedValue
    private int idBill;
    private BigDecimal totalPrice;
    private String billNumber;
    private Date date;

    @ManyToOne
    @JoinColumn(name="idAccount")
    private Account account;

    @OneToMany(mappedBy = "bill")
    private List<Historic> lHistoric;

    public Bill(){}

    public Bill(BigDecimal totalPrice, String billNumber, Account account, List<Historic> lHistoric) {
        this.totalPrice = totalPrice;
        this.billNumber = billNumber;
        this.account = account;
        this.lHistoric = lHistoric;
    }

    public Bill(Account account, List<Historic> lHistoric, Date date) {
        this.account = account;
        this.lHistoric = lHistoric;
        this.date = date;
    }

    public BigDecimal calculateTotalPrice(){
        BigDecimal total = new BigDecimal(0);
        for(Historic h : lHistoric){
            Timestamp endTimestamp = h.getRealEndTimestamp();
            Timestamp startTimestamp = h.getRealStartTimestamp();
            long milliseconds = endTimestamp.getTime() - startTimestamp.getTime();
            Double seconds = new Double(milliseconds / 1000);
            BigDecimal hours = new BigDecimal(seconds/3600).setScale(2,RoundingMode.FLOOR);
            BigDecimal totalForOne = h.getCar().getCar_model().getHour_price().multiply(hours);
            total = total.add(totalForOne.setScale(2));
        }
        return total;
    }

    public int getIdBill() {
        return idBill;
    }

    public void setIdBill(int idBill) {
        this.idBill = idBill;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(String billNumber) {
        this.billNumber = billNumber;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<Historic> getlHistoric() {
        return lHistoric;
    }

    public void setlHistoric(List<Historic> lHistoric) {
        this.lHistoric = lHistoric;
    }
}
