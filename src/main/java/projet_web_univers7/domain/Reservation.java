package projet_web_univers7.domain;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

@Entity
public class Reservation {
    @Id
    @GeneratedValue
    private int idReservation;
    private Timestamp startTimestamp;
    //private String realStartTimestamp;
    private Timestamp previsionEndTimestamp;
    //private String realEndTimestamp;
    private String state;


    @ManyToOne
    @JoinColumn(name="idPenalty")
    private Penalty penalty;

    @ManyToOne
    @JoinColumn(name="startPlace")
    private Place startPlace;

    @ManyToOne
    @JoinColumn(name="endPlace")
    private Place endPlace;

    @ManyToOne
    @JoinColumn(name="idCar")
    private Car car ;

    @ManyToOne
    @JoinColumn(name="idAccount")
    private Account account;

    public Reservation() {}


    public Reservation(int idReservation, Timestamp startTimestamp, Timestamp realstartTimestamp,
			Timestamp previsionEndTimestamp, Timestamp realEndTimestamp, String state, Penalty penalty,
			Place startPlace, Place endPlace, Car car, Account account) {
		super();
		this.idReservation = idReservation;
		this.startTimestamp = startTimestamp;
		//this.realstartTimestamp = realstartTimestamp;
		this.previsionEndTimestamp = previsionEndTimestamp;
		//this.realEndTimestamp = realEndTimestamp;
		this.state = state;
		this.penalty = penalty;
		this.startPlace = startPlace;
		this.endPlace = endPlace;
		this.car = car;
		this.account = account;
	}

    
	public Reservation(Timestamp startTimestamp, Timestamp realstartTimestamp, Timestamp previsionEndTimestamp,
			Timestamp realEndTimestamp, String state, Penalty penalty, Place startPlace, Place endPlace, Car car,
			Account account) {
		super();
		this.startTimestamp = startTimestamp;
		//this.realstartTimestamp = realstartTimestamp;
		this.previsionEndTimestamp = previsionEndTimestamp;
		//this.realEndTimestamp = realEndTimestamp;
		this.state = state;
		this.penalty = penalty;
		this.startPlace = startPlace;
		this.endPlace = endPlace;
		this.car = car;
		this.account = account;
	}
	
	public Reservation(Timestamp startTimestamp, Timestamp endTimestamp,  Place endPlace, Account a) {
		this.startTimestamp = startTimestamp;
		this.previsionEndTimestamp = endTimestamp;
		this.endPlace = endPlace;
		this.account = a;
	}

	
	public Reservation(Timestamp startTimestamp, Timestamp previsionEndTimestamp) {
		super();
		this.startTimestamp = startTimestamp;
		this.previsionEndTimestamp = previsionEndTimestamp;
	}


	public int getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(int idReservation) {
        this.idReservation = idReservation;
    }

    /*public String getPrevisionStartTimestamp() {
        return previsionStartTimestamp;
    }

    public void setPrevisionStartTimestamp(String startTimestamp) {
        this.previsionStartTimestamp = startTimestamp;
    }

    public String getRealStartTimestamp() {
        return realStartTimestamp;
    }

    public void setRealStartTimestamp(String realstartTimestamp) {
        this.realStartTimestamp = realstartTimestamp;
    }
*/
    public Timestamp getPrevisionEndTimestamp() {
        return previsionEndTimestamp;
    }

    public void setPrevisionEndTimestamp(Timestamp previsionEndTimestamp) {
        this.previsionEndTimestamp = previsionEndTimestamp;
    }

    /*public String getRealEndTimestamp() {
        return realEndTimestamp;
    }

    public void setRealEndTimestamp(String realEndTimestamp) {
        this.realEndTimestamp = realEndTimestamp;
    }
*/
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    public Place getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(Place startPlace) {
        this.startPlace = startPlace;
    }

    public Place getEndPlace() {
        return endPlace;
    }

    public void setEndPlace(Place endPlace) {
        this.endPlace = endPlace;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }


	public Timestamp getStartTimestamp() {
		return startTimestamp;
	}


	public void setStartTimestamp(Timestamp startTimestamp) {
		this.startTimestamp = startTimestamp;
	}
    
    
}
