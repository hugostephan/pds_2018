package projet_web_univers7.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Parking {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String name;
    private int capacity;
/*    @ManyToOne
    @JoinColumn(name="idSector")
    private Sector sector;*/
    private String address;
    private String longitude;
    private String latitude;
    @OneToMany(fetch = FetchType.EAGER,mappedBy="parking",cascade = CascadeType.ALL)
    private List<Place> placeCollection;

    public Long getId() {
        return id;
    }

    public List<Place> getPlaceCollection() {
        return placeCollection;
    }

    public void setPlaceCollection(List<Place> placeCollection) {
        this.placeCollection = placeCollection;
    }

    public String getName() {
        return name;
    }

    public int getCapacity() {
        return capacity;
    }

/*    public Sector getSector() {
        return sector;
    }*/

    public String getAddress() {
        return address;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

/*    public void setSector(Sector sector) {
      this.sector = sector;
    }*/

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Parking{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", capacity=" + capacity +
                ", address='" + address + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                '}';
    }
}
