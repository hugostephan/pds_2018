package projet_web_univers7.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity(name="car_model")
public class CarModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_car_model;
	private String model;
	private String brand;
	private String gas_consumption;
	private BigDecimal hour_price;
	private String picture;
	private String type;
	private int capacity;
	@OneToMany(fetch = FetchType.EAGER,mappedBy="car_model",cascade = CascadeType.ALL)
	private List<Car> car;
	
	public CarModel() {}

	public CarModel(int id_car_model, String model, String brand, String gas_consumption, BigDecimal hour_price,
			String picture, String type, int capacity, List<Car> car) {
		super();
		this.id_car_model = id_car_model;
		this.model = model;
		this.brand = brand;
		this.gas_consumption = gas_consumption;
		this.hour_price = hour_price;
		this.picture = picture;
		this.type = type;
		this.capacity = capacity;
		this.car = car;
	}
	public CarModel(int id_car_model, String model, String brand, String gas_consumption, BigDecimal hour_price,
			String picture, String type, int capacity) {
		super();
		this.id_car_model = id_car_model;
		this.model = model;
		this.brand = brand;
		this.gas_consumption = gas_consumption;
		this.hour_price = hour_price;
		this.picture = picture;
		this.type = type;
		this.capacity=capacity;
	}
	
	public CarModel(String brand, String model, String gas_consumption, BigDecimal hour_price,String picture, String type, int capacity) {
		
		this.model = model;
		this.brand = brand;
		this.gas_consumption = gas_consumption;
		this.hour_price = hour_price;
		this.picture = picture;
		this.type = type;
		this.capacity = capacity;
	}

	public int getId_car_model() {
		return id_car_model;
	}

	public void setId_car_model(int id_car_model) {
		this.id_car_model = id_car_model;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getGas_consumption() {
		return gas_consumption;
	}

	public void setGas_consumption(String gas_consumption) {
		this.gas_consumption = gas_consumption;
	}

	public BigDecimal getHour_price() {
		return hour_price;
	}

	public void setHour_price(BigDecimal hour_price) {
		this.hour_price = hour_price;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public List<Car> getCar() {
		return car;
	}

	public void setCar(List<Car> car) {
		this.car = car;
	}
	
	
	@Override
	public String toString() {
		return "Car_model [id_car_model=" + id_car_model + ", model=" + model + ", brand=" + brand
				+ ", gas_consumption=" + gas_consumption + ", hour_price=" + hour_price + ", picture=" + picture
				+ "type=" +type + ", capacity="+ capacity +"]";
	}
	
	
	
}
