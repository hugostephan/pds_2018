package projet_web_univers7.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
public class Penalty {

    @Id
    @GeneratedValue
    private int id;
    @Size(min=0, max=1)
    private float percentage;
    @OneToMany(mappedBy = "penalty")
    private List<Reservation> lReservation;

    public Penalty() {
    }

    public Penalty(float percentage, List<Reservation> lReservation) {
        this.percentage = percentage;
        this.lReservation = lReservation;
    }

    public Penalty(float percentage) {
        this.percentage = percentage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPercentage() {
        return percentage;
    }

    public void setPercentage(float percentage) {
        this.percentage = percentage;
    }

    public List<Reservation> getlReservation() {
        return lReservation;
    }

    public void setlReservation(List<Reservation> lReservation) {
        this.lReservation = lReservation;
    }
}
