package projet_web_univers7.domain;

import javax.persistence.*;

@Entity
public class Sector {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private String town;
    private String postalCode;
    @ManyToOne
    @JoinColumn(name="idTypeSector")
    private TypeSector typeSector;

    public TypeSector getTypeSector() {
        return typeSector;
    }

    public void setTypeSector(TypeSector typeSector) {
        this.typeSector = typeSector;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

}