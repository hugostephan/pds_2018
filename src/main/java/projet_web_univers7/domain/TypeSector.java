package projet_web_univers7.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Digits;
import java.util.List;

@Entity
public class TypeSector {

    @Id
    @GeneratedValue
    private int id;
    private String color;
    @Digits(integer=1, fraction=2)

    private double percent;
    @OneToMany(mappedBy = "typeSector")
    private List<Sector> lSector;

    public TypeSector(){}

    public TypeSector(String color, double percent){
        this.color = color;
        this.percent = percent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }
}
