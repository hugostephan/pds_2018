package projet_web_univers7.domain;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Historic {

    @Id
    @GeneratedValue
    private int idHistoric;
    private Timestamp previsionStartTimestamp;
    private Timestamp previsionEndTimestamp;
    private String state;
    private Timestamp realStartTimestamp;
    private Timestamp realEndTimestamp;

    @ManyToOne
    @JoinColumn(name="idPenalty")
    private Penalty penalty;


    @ManyToOne
    @JoinColumn(name="startPlace")
    private Place startPlace;

    @ManyToOne
    @JoinColumn(name="endPlace")
    private Place endPlace;

    @ManyToOne
    @JoinColumn(name="idCar")
    private Car car ;

    @ManyToOne
    @JoinColumn(name="idAccount")
    private Account account;

    @ManyToOne
    @JoinColumn(name="idBill")
    private Bill bill;

    public Historic(){

    }

    public Historic(Timestamp previsionStartTimestamp, Timestamp previsionEndTimestamp, String state, Timestamp realStartTimestamp, Timestamp realEndTimestamp, Penalty penalty, Place startPlace, Place endPlace, Car car, Account account, Bill bill) {
        this.previsionStartTimestamp = previsionStartTimestamp;
        this.previsionEndTimestamp = previsionEndTimestamp;
        this.state = state;
        this.realStartTimestamp = realStartTimestamp;
        this.realEndTimestamp = realEndTimestamp;
        this.penalty = penalty;
        this.startPlace = startPlace;
        this.endPlace = endPlace;
        this.car = car;
        this.account = account;
        this.bill = bill;
    }

    public int getIdHistoric() {
        return idHistoric;
    }

    public void setIdHistoric(int idHistoric) {
        this.idHistoric = idHistoric;
    }

    public Timestamp getPrevisionStartTimestamp() {
        return previsionStartTimestamp;
    }

    public void setPrevisionStartTimestamp(Timestamp previsionStartTimestamp) {
        this.previsionStartTimestamp = previsionStartTimestamp;
    }

    public Timestamp getPrevisionEndTimestamp() {
        return previsionEndTimestamp;
    }

    public void setPrevisionEndTimestamp(Timestamp previsionEndTimestamp) {
        this.previsionEndTimestamp = previsionEndTimestamp;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Timestamp getRealStartTimestamp() {
        return realStartTimestamp;
    }

    public void setRealStartTimestamp(Timestamp realStartTimestamp) {
        this.realStartTimestamp = realStartTimestamp;
    }

    public Timestamp getRealEndTimestamp() {
        return realEndTimestamp;
    }

    public void setRealEndTimestamp(Timestamp realEndTimestamp) {
        this.realEndTimestamp = realEndTimestamp;
    }

    public Penalty getPenalty() {
        return penalty;
    }

    public void setPenalty(Penalty penalty) {
        this.penalty = penalty;
    }

    public Place getStartPlace() {
        return startPlace;
    }

    public void setStartPlace(Place startPlace) {
        this.startPlace = startPlace;
    }

    public Place getEndPlace() {
        return endPlace;
    }

    public void setEndPlace(Place endPlace) {
        this.endPlace = endPlace;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }
}
