package projet_web_univers7.domain;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Course {

	@Id
    @GeneratedValue(strategy= GenerationType.AUTO)
	private int id;
	@OneToOne
	private Reservation id_resa;
	private Timestamp realStartTimestamp;
	private Timestamp realEndTimestamp;
	private String longitude;
    private String latitude;
    private Boolean late;
    
    
    public Course() {}
    
    public Course(Reservation id_resa, Timestamp realStartTimestamp, Timestamp realEndTimestamp, String longitude,
			String latitude) {
		this.id_resa = id_resa;
		this.realStartTimestamp = realStartTimestamp;
		this.realEndTimestamp = realEndTimestamp;
		this.longitude = longitude;
		this.latitude = latitude;
	}
    
	public Course(int id, Reservation id_resa, Timestamp realStartTimestamp, Timestamp realEndTimestamp, String longitude,
			String latitude) {
		this.id = id;
		this.id_resa = id_resa;
		this.realStartTimestamp = realStartTimestamp;
		this.realEndTimestamp = realEndTimestamp;
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	
	
	public Boolean getLate() {
		return late;
	}

	public void setLate(Boolean late) {
		this.late = late;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Reservation getId_resa() {
		return id_resa;
	}
	public void setId_resa(Reservation id_resa) {
		this.id_resa = id_resa;
	}
	public Timestamp getRealStartTimestamp() {
		return realStartTimestamp;
	}
	public void setRealStartTimestamp(Timestamp realStartTimestamp) {
		this.realStartTimestamp = realStartTimestamp;
	}
	public Timestamp getRealEndTimestamp() {
		return realEndTimestamp;
	}
	public void setRealEndTimestamp(Timestamp realEndTimestamp) {
		this.realEndTimestamp = realEndTimestamp;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	@Override
	public String toString() {
		return "Course [id=" + id + ", id_resa=" + id_resa + ", realStartTimestamp=" + realStartTimestamp
				+ ", realEndTimestamp=" + realEndTimestamp + ", longitude=" + longitude + ", latitude=" + latitude
				+ "]";
	}
    
    
}
