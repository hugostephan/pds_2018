package projet_web_univers7.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projet_web_univers7.domain.Sector;

@Repository
public interface SectorRepository extends JpaRepository<Sector, Integer>{
}
