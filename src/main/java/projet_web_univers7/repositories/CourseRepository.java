package projet_web_univers7.repositories;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import projet_web_univers7.domain.Course;


@Repository
public interface CourseRepository extends CrudRepository<Course, Integer> {

	 @Query("from Course where :now >= realStartTimestamp AND :now <= realEndTimestamp")
	 public List<Course> findAllCourses(@Param("now") Timestamp now);
	 
	 @Modifying
	 @Query("Update Course set realEndTimestamp = :real where id = :id")
	 public void setRealEndTimestamp(@Param("real") Timestamp real, @Param("id") int id);
}
