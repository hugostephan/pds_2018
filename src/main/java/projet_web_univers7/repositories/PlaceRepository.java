package projet_web_univers7.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;

import java.util.List;

@Repository
public interface PlaceRepository extends CrudRepository<Place, Integer> {
    List<Place> findAllByParking (Parking parking);
    
    @Query("from Place where name = :name")
    Place getByName(@Param("name")String name);
}
