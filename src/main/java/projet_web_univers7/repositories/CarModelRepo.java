package projet_web_univers7.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import projet_web_univers7.domain.CarModel;



@Repository
public interface CarModelRepo extends CrudRepository<CarModel, Integer>{

	 @Query("select distinct c.brand from car_model c")
	 public List<String> getByBrand();
	 
	 @Query("select distinct c.model from car_model c")
	 public List<String> getByModel();
	 
	 @Query("select distinct c.type from car_model c")
	 public List<String> getByType();
	 
	 @Query("select distinct c.capacity from car_model c")
	 public List<String> getByCapacity();
	 
	 @Query("select distinct c.gas_consumption from car_model c")
	 public List<String> getByGs();
	 
	 @Query("from car_model where brand = :brand and model = :model")
	 public CarModel getCarModelByBrandModel(@Param("brand")String brand,@Param("model") String model);
	 
	 @Query("select distinct c.model, c.brand from car_model c")
	 public List<String> getModelName();
}
