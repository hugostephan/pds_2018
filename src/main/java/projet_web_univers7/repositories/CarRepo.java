package projet_web_univers7.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import projet_web_univers7.domain.Car;

//Author : Nassim

@Repository
public interface CarRepo extends CrudRepository<Car, Integer> {

	 @Query("select distinct c.licence_number from car c")
	 public List<String> getByLn();
	 
	 @Query("select distinct c.created_year from car c")
	 public List<String> getByYear();
}
