package projet_web_univers7.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projet_web_univers7.domain.Penalty;

@Repository
public interface PenaltyRepository extends JpaRepository<Penalty, Integer>{
}
