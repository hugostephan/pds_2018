package projet_web_univers7.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import projet_web_univers7.domain.Historic;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface HistoricRepository extends JpaRepository<Historic, Integer> {
    @Query(value = "SELECT count(idHistoric) FROM Historic")
    int getNumberHistoric();


    @Query(value="select * from historic where historic.id_account = :idAccount and historic.real_end_timestamp between :dateStart and :dateEnd", nativeQuery = true)
    List<Historic> findAllByAccount_IdAndRealEndTimestampBetween(@Param("idAccount") int idAccount, @Param("dateStart") Timestamp dateStart, @Param("dateEnd") Timestamp dateEnd);
}
