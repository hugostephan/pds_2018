package projet_web_univers7.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;

public interface ParkingRepository extends JpaRepository<Parking, Integer> {
    Parking findById(Long id);
}
