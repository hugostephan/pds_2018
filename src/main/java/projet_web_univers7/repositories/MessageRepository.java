package projet_web_univers7.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import projet_web_univers7.domain.Message;

public interface MessageRepository extends JpaRepository<Message, Integer> {
}
