package projet_web_univers7.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import projet_web_univers7.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
