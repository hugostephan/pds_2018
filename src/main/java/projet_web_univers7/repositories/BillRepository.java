package projet_web_univers7.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import projet_web_univers7.domain.Bill;

@Repository
public interface BillRepository extends JpaRepository<Bill, Integer> {
}
