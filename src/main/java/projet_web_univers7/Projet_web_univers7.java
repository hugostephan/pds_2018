package projet_web_univers7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class Projet_web_univers7 {


    public static void main(String[] args){
        SpringApplication.run(Projet_web_univers7.class, args);
    }
}
