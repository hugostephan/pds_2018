package projet_web_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import projet_web_univers7.domain.Account;
import projet_web_univers7.domain.Role;
import projet_web_univers7.services.AccountService;
import projet_web_univers7.services.RoleService;
import projet_web_univers7.services.SecurityServiceImpl;
import javax.validation.Valid;


/**
 * Account Controller, handles mapping, model queries and view displaying for user accounts
 * /login, /registration and /myaccount URIs
 * @author Hugo STEPHAN
 */
@Controller
public class AccountController
{
    // Autowired services
    @Autowired
    private SecurityServiceImpl securityService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private RoleService roleService;



    /**
     * Handles GET /registration, provide registration form
     * @param model Persistence Data Model
     * @return view name
     */
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new Account());
        return "registration";
    }

    /**
     * Handles POST /registration, validate user's entries and save account if everything's fine
     * @param userForm New Account Object
     * @param bindingResult Validation results
     * @param model Persistence Data Model
     * @return view name
     */
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") Account userForm, BindingResult bindingResult, Model model) {
        bindingResult = accountService.validateCreateAccount(userForm, bindingResult);
        if (bindingResult.hasErrors())
        {
            model.addAttribute("error", "Rejected data, please correct errors and re submit !");
            return "registration";
        }

        Role r_customer = roleService.getRoleByName("customer");
        userForm.setRole(r_customer);

        accountService.saveAccount(userForm);
        securityService.autologin(userForm.getUsername(), userForm.getPassword());

        model.addAttribute("username", userForm.getUsername());
        return "index";
    }

    /**
     * Login page, all unauthentified users are redirected to this page
     * @param model Persistence Data Model
     * @param error Potential login error
     * @param logout Does the user just logout ?
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and/or password are invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    /**
     * Handles GET /myaccount, provide account management form
     * @param model Persistence Data Model
     * @return view name
     */
    @RequestMapping(value = "/myaccount", method = RequestMethod.GET)
    public String myaccount_get(Model model)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            model.addAttribute("userForm", accountService.findByUsername(auth.getName()));
            System.out.println(auth.getName());
            return "myaccount";
        }
        else
        {
            return "index";
        }
    }

    /**
     * Handles POST /myaccount, validate user's modifications and save account if everything's fine
     * @param userForm Account Object
     * @param bindingResult Validation results
     * @param model Persistence Data Model
     * @return view name
     */
    @RequestMapping(value = "/myaccount", method = RequestMethod.POST)
    public String myaccount_post(@ModelAttribute("userForm") @Valid Account userForm, BindingResult bindingResult, Model model)
    {
        if(
                (!securityService.isAuthenticated()) ||
                (securityService.getAuthenticatedUser().getId() != userForm.getId() && (!securityService.isAdministrator())))
        {
            return "index";
        }
        bindingResult = accountService.validateUpdateAccount(userForm, bindingResult);
        if (bindingResult.hasErrors())
        {

            model.addAttribute("error", "Rejected data, please correct errors and re submit !");
            return "myaccount";
        }

        accountService.saveAccount(userForm);
        model.addAttribute("accountDetails", userForm);
        model.addAttribute("message", "Modifications saved");
        return "index";
    }
}