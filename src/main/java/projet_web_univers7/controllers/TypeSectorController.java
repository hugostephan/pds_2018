package projet_web_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import projet_web_univers7.domain.TypeSector;
import projet_web_univers7.services.TypeSectorService;
import projet_web_univers7.validator.TypeSectorValidator;

@Controller
public class TypeSectorController {

    @Autowired
    TypeSectorService typeSectorService;

    @Autowired
    TypeSectorValidator typeSectorWindowValidator;

    @GetMapping("/referentielTarifTS")
    public String priceRefControllerMain(Model model){
        model.addAttribute("listTypeSector", typeSectorService.getAllTypeSector());
        return "priceRefMain";
    }

    @PostMapping("/referentielTarifTS")
    public String updateTypeSector(@ModelAttribute TypeSector ts, Model model, BindingResult bindingResult){
        typeSectorWindowValidator.validate(ts, bindingResult);

        if (bindingResult.hasErrors())
        {
            model.addAttribute("error", "Rejected data, Make sure percent is positive and isn't > 0.5");
            model.addAttribute("typeSector", typeSectorService.getTypeSectorById(ts.getId()));
            return "typeSectorWindow";
        }
        typeSectorService.saveTypeSector(ts);
        model.addAttribute("message", "Data updated : success");
        model.addAttribute("listTypeSector", typeSectorService.getAllTypeSector());
        return "priceRefMain";
    }

    @RequestMapping("/typeSectorCreatePercent/{id}")
    public String typeSectorCreatePercent(Model model, @PathVariable(name="id") int id){
        model.addAttribute("typeSector", typeSectorService.getTypeSectorById(id));
        return "typeSectorWindow";
    }
}
