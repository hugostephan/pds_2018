package projet_web_univers7.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping(value = {  "/",  "/index"})
    String index(){
        return "index";
    }
}