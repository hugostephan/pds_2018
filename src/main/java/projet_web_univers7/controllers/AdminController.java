package projet_web_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import projet_web_univers7.domain.Account;
import projet_web_univers7.domain.Role;
import projet_web_univers7.services.AccountService;
import projet_web_univers7.services.SecurityService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Admin controller handles BO Admin special privileges such as managing customer accounts
 * /admin/* URIs
 * @author Hugo STEPHAN
 */
@Controller
public class AdminController {
    @Autowired
    private SecurityService securityService;
    @Autowired
    private AccountService accountService;
    Account account ;


    /**
     * Handles GET /admin/accounts provide accounts list if user is correctly authenticated as backoffice admin
     * @param model Persistence Data Model
     * @return view name
     */
    @RequestMapping(value = "/admin/accounts", method = RequestMethod.GET)
    public String manageAccounts(Model model)
    {
        if( (!securityService.isAuthenticated()) || (!securityService.isAdministrator()) )
        {
            return "index";
        }


        account = securityService.getAuthenticatedUser();
        List<Account> accounts = accountService.findAllByRole("customer");

        model.addAttribute("account",account);
        model.addAttribute("accounts",accounts);

        return "accounts";
    }


    /**
     * Handles GET /admin/accounts provide accounts list if user is correctly authenticated as backoffice admin
     * @param model Persistence Data Model
     * @return view name
     */
    @RequestMapping(value = "/admin/accounts", method = RequestMethod.POST)
    public String manageAccountsFilter(Model model,
                                       @RequestParam(value = "firstnameFilter", defaultValue = "PRENOM") String firstname,
                                       @RequestParam(value = "lastnameFilter",defaultValue = "NOM") String lastname,
                                       @RequestParam(value = "phoneNumberFilter", defaultValue = "TELEPHONE") String phoneNumber,
                                       @RequestParam(value = "cityFilter", defaultValue = "VILLE") String city)
    {
        if( (!securityService.isAuthenticated()) || (!securityService.isAdministrator()) )
        {
            return "index";
        }


        // Passing connected user info to view
        account = securityService.getAuthenticatedUser();
        model.addAttribute("acc",account);

        // Accounts to pass to the view
        List<Account> accounts ;

        // Funny user clicked "Filter" without specifying any filter
        if(firstname.equals("PRENOM") && lastname.equals("NOM") && phoneNumber.equals("TELEPHONE") && city.equals("VILLE")) {
            accounts = accountService.findAllByRole("customer");
        }
        else
        {
            HashMap <String,String> crit = new HashMap<String,String>();
            if(!firstname.equals("PRENOM")) { crit.put("PRENOM", firstname) ; }
            if(!lastname.equals("NOM")) { crit.put("NOM", lastname); }
            if(!phoneNumber.equals("TELEPHONE")) { crit.put("TELEPHONE", phoneNumber); }
            if(!city.equals("VILLE")) { crit.put("VILLE", city); }

            accounts = accountService.findCustomersWithFilters(crit);
        }

        model.addAttribute("accounts", accounts);
        return "accounts";
    }

    /**
     * Handles GET /admin/accounts provide accounts list if user is correctly authenticated as backoffice admin
     * @param model Persistence Data Model
     * @return view name
     */
    @RequestMapping(value = "/admin/account/{id}")
    public String manageAccount(Model model, @PathVariable String id)
    {
        if( (!securityService.isAuthenticated()) || (!securityService.isAdministrator()) )
        {
            return "index";
        }


        model.addAttribute("userForm", accountService.findById(id));
        return "myaccount";
    }
}
