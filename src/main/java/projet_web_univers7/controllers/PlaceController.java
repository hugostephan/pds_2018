package projet_web_univers7.controllers;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;
import projet_web_univers7.services.ParkingServiceImpl;
import projet_web_univers7.services.PlaceServiceImpl;
import projet_web_univers7.validator.PlaceValidator;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PlaceController {
    @Autowired
    private PlaceServiceImpl service;

    @Autowired
    private ParkingServiceImpl parkingService;

    // Autowired validators
    @Autowired
    private PlaceValidator placeValidator;

    @RequestMapping(value = "/parking/{id}/addplace/", method = RequestMethod.GET)
    public String newPlace(Model model,@PathVariable int id){
        model.addAttribute("place", new Place());
        model.addAttribute("idparking",id);
        return "placeform";
    }

    @RequestMapping(value = "/parking/{id}/places/", method = RequestMethod.GET)
    public String listPlace(@PathVariable int id, Model model){
        Parking parking = new Parking();
        parking.setId(new Long(id));
        model.addAttribute("places", service.listPlaceByParking(parking));
        model.addAttribute("idparking",id);
        return "parkingplaces";
    }

    @RequestMapping(value = "/parking/{id}/addplace/", method = RequestMethod.POST)
    public String addPlace(@ModelAttribute("place")Place place, @PathVariable int id, BindingResult bindingResult, @RequestParam(value="action", required=true) String action,Model model)
    {
        String ret ;

        placeValidator.validate(place, bindingResult);

        if (bindingResult.hasErrors())
        {
            model.addAttribute("error", "Rejected data, please fill all the fields correctly. " +
                    "Name must have between 0 and 3 characters. " +
                    "Width and Lenght must have characters");
            model.addAttribute("place", new Place());
            model.addAttribute("idparking",id);
            return "placeform";
        }


        // Save product only if submit event was triggered by saveAccount button
        if (action.equals("save"))
        {
            service.addPlace(place,id);
            ret = "redirect:/parkings/";//"redirect:/message/" + message.getId();
        }
        else
        {
            ret = "redirect:/";
        }

        return ret ;
    }



    @RequestMapping(value = "/parking/{id}/addplace/{id}", method = RequestMethod.POST)
    public String addPlace(Model model){
        return "addplaceform";
    }

    @RequestMapping(value = "/searchplace", method = RequestMethod.GET)
    public String searchPlace(Model model){
        Authentication auth
                = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            model.addAttribute("listParking", parkingService.listAllParking());
            model.addAttribute("listPlaces", service.listAll());
            return "searchplace";
        }
        else
        {
            return "index";
        }
    }

    @RequestMapping(value = "/searchplace", method = RequestMethod.POST)
    public String searchPlacePost(@RequestParam(value="parking") String parking, @RequestParam(value="placename") String placeName, @RequestParam(value="width") String width ,@RequestParam(value="length") String length, Model model){

        List<Place> listPlace;
        ArrayList<Place> res = new ArrayList<Place>();
        listPlace= service.listAll();

        if (parking=="" && placeName=="" && width=="" && length=="") {
            res.addAll(listPlace);
        }
        else {
            for (Place place : listPlace) {

                if ((place.getName().equalsIgnoreCase(placeName) || placeName=="") &&
                        ( place.getWidth().equalsIgnoreCase(width) || width=="" )
                        && (place.getParking().getName().equalsIgnoreCase(parking) || parking=="")
                        && (place.getLength().equalsIgnoreCase(length) || length=="")) {
                    res.add(place);
                }
            }
        }


        model.addAttribute("listParking", parkingService.listAllParking());
        model.addAttribute("listPlaces", res);
        return "searchplace";
    }
}
