package projet_web_univers7.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import projet_web_univers7.domain.CarModel;
import projet_web_univers7.services.CarModelService;
import projet_web_univers7.validator.CarModelValidator;


@Controller
public class CarModelController {

    @Autowired
    private CarModelService carModelService;

    @Autowired
    private CarModelValidator carModelValidator;

    @Autowired
    public void setMessageService(CarModelService carModelservice) {
        this.carModelService = carModelService;
    }

    @GetMapping(value = "/add_model")
    public String greetingForm(Model model) {
        model.addAttribute("carModel", new CarModel());
        return "add_model";
    }

    @PostMapping(value = "/add_model")
    public String addModel(@ModelAttribute CarModel cm, Model model) {
        carModelService.insertModel(cm);
        model.addAttribute("allModels", carModelService.getAllModels());
        return "models";
    }

    @GetMapping("/referentielTarifCM")
    public String priceRefCM(Model model) {
        model.addAttribute("listCarModel", carModelService.findAllModel());
        model.addAttribute("allModels", carModelService.getAllModels());
        model.addAttribute("brandName",carModelService.getDistinctBrand());

        return "priceRefListCarModel";
    }

    @PostMapping("/referentielTarifCM")
    public String updateCM(Model model, @ModelAttribute CarModel carModel, BindingResult bindingResult){
        carModelValidator.validate(carModel, bindingResult);

        if (bindingResult.hasErrors())
        {
            model.addAttribute("error", "Rejected data, Make sure price_hour is positive and isn't > 999.99");
            model.addAttribute("carModel", carModelService.getCarModelById(carModel.getId_car_model()));
            return "carModelWindow";
        }
        carModelService.insertModel(carModel);
        model.addAttribute("message", "Data updated : success");
        model.addAttribute("listCarModel", carModelService.findAllModel());
        return "priceRefListCarModel";
    }

    @RequestMapping("/carModelCreatePriceHour/{id}")
    public String carModelCreatePriceHour(Model model, @PathVariable(name = "id") int id) {
        model.addAttribute("carModel", carModelService.getCarModelById(id));
        return "carModelWindow";
    }



    @RequestMapping(value ="/searchReferentielTarifCM",method = RequestMethod.POST)
    public String search(@RequestParam(value="brand") String brand, @RequestParam(value="model") String modelVehicle, @RequestParam(value="gas_consumption") String gasConsumption,
                         @RequestParam(value="hourPriceBegin") BigDecimal hourPriceBegin,@RequestParam(value="hourPriceEnd") BigDecimal hourPriceEnd,Model model){
        List<CarModel> listModel;
        ArrayList<CarModel> res = new ArrayList<CarModel>();
        listModel= carModelService.findAllModel();

        if (brand=="" && modelVehicle=="" && gasConsumption=="" && hourPriceBegin==null && hourPriceEnd==null) {
            res.addAll(listModel);
        }
        else {
            for (CarModel c : listModel) {

                if ((c.getBrand().equalsIgnoreCase(brand) || brand=="") &&
                        ( c.getModel().equalsIgnoreCase(modelVehicle) || modelVehicle=="" )
                        && (c.getGas_consumption().equalsIgnoreCase(gasConsumption) || gasConsumption=="")
                    && (hourPriceBegin==null ||(c.getHour_price().compareTo(hourPriceBegin)>=0))
                    && (hourPriceEnd==null ||(c.getHour_price().compareTo(hourPriceEnd)<=0)))
                {
                    res.add(c);
                }
            }
        }

        model.addAttribute("listCarModel", res);
        model.addAttribute("allModels",carModelService.getAllModels());
        model.addAttribute("brandName",carModelService.getDistinctBrand());

        return "priceRefListCarModel";
    }

}
