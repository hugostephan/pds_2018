package projet_web_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import projet_web_univers7.services.ParkingServiceImpl;

@Controller
public class ParkingController {
    @Autowired
    private ParkingServiceImpl service;

    @RequestMapping(value = "/parkings", method = RequestMethod.GET)
    public String list(Model model){
        Authentication auth
                = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            model.addAttribute("parkings", service.listAllParking());
            return "parkings";
        }
        else
        {
            return "index";
        }


    }

    @RequestMapping("parking/{id}")
    public String showMessage(@PathVariable Integer id, Model model){
        model.addAttribute("parking", service.getParkingById(id));
        return "parkingshow";
    }



}
