package projet_web_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import projet_web_univers7.domain.Account;
import projet_web_univers7.domain.Bill;
import projet_web_univers7.domain.Historic;
import projet_web_univers7.services.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Controller
public class BillController {

    @Autowired
    private PenaltyService penaltyService;

    @Autowired
    private HistoricService historicService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private BillService billService;

    @Autowired
    private SecurityService securityService;

    @RequestMapping("mainActivity")
    public String mainActivity(){
        return "activity";
    }

    @RequestMapping("getActivity")
    public String getActivityFor30Days(Model model, @RequestParam("date") String dateString){

        if ((!securityService.isAuthenticated()) || (!securityService.isAdministrator())) {
            return "index";
        }

        List<Bill> lBillResult = new ArrayList<Bill>();
        if(dateString != null && dateString != "") {
            //initDate
            Timestamp timestampStart = null;
            Timestamp timestampEnd = null;
            try{
                SimpleDateFormat formatterEN = new SimpleDateFormat("yyyy-MM-dd");
                Date dateEn = formatterEN.parse(dateString);
                SimpleDateFormat formatterFR = new SimpleDateFormat("dd-MM-yyyy");
                String date = formatterFR.format(dateEn);
                Calendar cal = Calendar.getInstance();
                cal.setTime(formatterFR.parse(date));
                cal.add(Calendar.DATE, -30);
                Date dateMinus30Days = cal.getTime();
                SimpleDateFormat dateFormatTimestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String stringTimestampEnd = dateFormatTimestamp.format(dateEn);
                timestampEnd = new Timestamp(dateFormatTimestamp.parse(stringTimestampEnd).getTime());
                String stringTimestampStart = dateFormatTimestamp.format(dateMinus30Days);
                timestampStart = new Timestamp(dateFormatTimestamp.parse(stringTimestampStart).getTime());
            }catch (ParseException pe){
                pe.getStackTrace();
            }

            //get all account wich are registered at the same day
            List<Account> lAccount = accountService.findCustomersByRegistritionDate(dateString.split("-")[2]+"%");

            for (Account a : lAccount) {
                List<Historic> lhistoric = historicService.findHistoricsByAccountAndDate(a.getId(), timestampStart, timestampEnd);
                if(lhistoric.size() != 0) {
                    Historic historic = lhistoric.get(0);
                    Bill b = new Bill(a, lhistoric, Calendar.getInstance().getTime());
                    b.setTotalPrice(b.calculateTotalPrice());
                    b = billService.saveBill(b);
                    for (Historic h : lhistoric) {
                        h.setBill(b);
                        historicService.saveHistoric(h);
                    }
                    lBillResult.add(b);
                }
            }
        }
        model.addAttribute("listBill", lBillResult);
        return "activity";
    }

    @RequestMapping("/bill/{idBill}")
    public String detailBill(Model model, @PathVariable("idBill") String idBill) {
        if ((!securityService.isAuthenticated()) || (!securityService.isAdministrator())) {
            return "index";
        }
        model.addAttribute("bill", billService.findById(Integer.parseInt(idBill)));
        return "billDetails";
    }

}
