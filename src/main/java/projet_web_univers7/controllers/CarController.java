package projet_web_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import projet_web_univers7.domain.Car;
import projet_web_univers7.domain.CarModel;
import projet_web_univers7.services.CarModelService;
import projet_web_univers7.services.CarService;



@Controller
public class CarController {
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private CarModelService carModelService;
	
	@Autowired
    public void setMessageService(CarService vehicleService, CarModelService carModelService) {
        this.carService = vehicleService;
        this.carModelService = carModelService;
    }
	
	//Add
	@GetMapping(value = "/addcar")
	public String greetingForm(final Model model) {
        model.addAttribute("car", new Car());
        model.addAttribute("allModels", carModelService.getAllModels());
        model.addAttribute("brandName",carModelService.getDistinctBrand());
        model.addAttribute("model", new CarModel());
        return "add_car";
    }
	
	@PostMapping(value = "/add") 
    public String addCar(@ModelAttribute Car v, @RequestParam(value="brand") String brand, @RequestParam(value="model") String modelVehicle, final Model model){
		CarModel setModel = carModelService.findCarModelByBrand(brand,modelVehicle);
		v.setCar_model(setModel);
	    Car c =	carService.insertCar(v);
	    System.out.println(c.getLicence_number());
	    if(c.getLicence_number()== null) {
	    
	    		model.addAttribute("already", true);
	    }
	    	model.addAttribute("allCars",carService.getAllCars());
	    	return "cars";
    }
	
	//Select 
	@GetMapping(value = "/selectcar")
	public String selectCar(final Model model) {
        model.addAttribute("car", new Car());
        model.addAttribute("allCars", carService.getAllCars());
        model.addAttribute("brandName",carModelService.getDistinctBrand());
        model.addAttribute("models",carModelService.getDistinctModel());
        model.addAttribute("types",carModelService.getDistinctType());
        model.addAttribute("capacities",carModelService.getDistinctCapacity());
        model.addAttribute("gss",carModelService.getDistinctGs());
        model.addAttribute("lns",carService.getDistinctLn());
        model.addAttribute("years",carService.getDistinctYear());
        return "search_car";
    }
	
	
	
	
	
}
