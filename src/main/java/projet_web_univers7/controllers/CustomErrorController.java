package projet_web_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Custom Error Controller
 * Override Spring Boot default error controller to display custom error page
 * @author Hugo STEPHAN
 */
@Controller
public class CustomErrorController implements ErrorController {

    @Autowired
    private ErrorAttributes errorAttributes;

    private static final String PATH = "/error";

    @RequestMapping(value = "/error")
    public String error(HttpServletRequest request, HttpServletResponse response, Model model) {
        Map<String,Object> m = getErrorAttributes(request, true);
        model.addAttribute("method", request.getMethod());
        model.addAttribute("status", response.getStatus());
        model.addAttribute("stack", m.toString());
        return "error_custom";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
    }
}