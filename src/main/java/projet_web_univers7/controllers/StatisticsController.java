package projet_web_univers7.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import projet_web_univers7.domain.Historic;
import projet_web_univers7.domain.MonthStat;
import projet_web_univers7.services.DelayService;
import projet_web_univers7.services.DelayServiceImpl;
import projet_web_univers7.services.StatisticService;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.Month;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Statistics controller : handles BO Admin Statistic request
 * /stats/* URIs
 * @author Quentin GOUTTE, Hugo STEPHAN
 */
@Controller
public class StatisticsController {
    @Autowired
    private DelayService delayService;
    @Autowired
    private StatisticService statisticService;

    // Format variables
    private static DecimalFormat decimalFormat = new DecimalFormat(".##");

    /**
     * Parent view : statsmenu
     * @param model Persistence Data Model
     * @return view's name
     */
    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public String menuStats(Model model){
        return "statsmenu";
    }

    /**
     * Display number and rate of delay
     * @param model Persistence Data Model
     * @return view's name
     */
    @RequestMapping(value = "/stats/retards", method = RequestMethod.GET)
    public String getStatsDelayRate(Model model) {
        ArrayList<MonthStat> listStat = delayService.getStatByMonth();

        model.addAttribute("listStat", listStat);
        model.addAttribute("nbDelay", delayService.getNumberDelay());
        model.addAttribute("rateDelay", format(delayService.getRateDelay() * 100));
        model.addAttribute("nbHisto", delayService.getNumberHistoric());
        return "statsretard";
    }

    /**
     * Display advanced statistic view (mean, variance, abnormal data, corrected mean & variance)
     * @param model Persistence Data Model
     * @return view's name
     */
    @RequestMapping(value = "/stats/analyse")
    public String getDelayAnalyse(Model model)
    {
        double[] series = delayService.getDelayedSeries();

        double [] abnormal_series_2s = statisticService.getDataBiggerThan2Sigma(series);
        double [] purged_series_2s = statisticService.purgeExeceptionalData(series, 2);

        double [] abnormal_series_3s = statisticService.getDataBiggerThan3Sigma(series);
        double [] purged_series_3s = statisticService.purgeExeceptionalData(series, 3);

        model.addAttribute("delay_list", series);
        model.addAttribute("delay_list_size", series.length);
        model.addAttribute("delay_mean", format(statisticService.getMean(series)));
        model.addAttribute("delay_variance", format(statisticService.getVariance(series)));
        model.addAttribute("delay_sigma", format(statisticService.getSigma(series)));


        // Data > 2 sigmas
        model.addAttribute("abnormal_list_2", abnormal_series_2s);
        model.addAttribute("abnormal_list_size_2", abnormal_series_2s.length);
        model.addAttribute("abnormal_mean_2", format(statisticService.getMean(abnormal_series_2s)));
        model.addAttribute("abnormal_variance_2", format(statisticService.getVariance(abnormal_series_2s)));
        model.addAttribute("abnormal_sigma_2", format(statisticService.getSigma(abnormal_series_2s)));

        model.addAttribute("purged_series_2", purged_series_2s);
        model.addAttribute("purged_series_size_2", purged_series_2s.length);
        model.addAttribute("purged_mean_2", format(statisticService.getMean(purged_series_2s)));
        model.addAttribute("purged_variance_2", format(statisticService.getVariance(purged_series_2s)));
        model.addAttribute("purged_sigma_2", format(statisticService.getSigma(purged_series_2s)));


        // Data > 3 sigmas
        model.addAttribute("abnormal_list_3", abnormal_series_3s);
        model.addAttribute("abnormal_list_size_3", abnormal_series_3s.length);
        model.addAttribute("abnormal_mean_3", format(statisticService.getMean(abnormal_series_3s)));
        model.addAttribute("abnormal_variance_3", format(statisticService.getVariance(abnormal_series_3s)));
        model.addAttribute("abnormal_sigma_3", format(statisticService.getSigma(abnormal_series_3s)));

        model.addAttribute("purged_series_3", purged_series_3s);
        model.addAttribute("purged_series_size_3", purged_series_3s.length);
        model.addAttribute("purged_mean_3", format(statisticService.getMean(purged_series_3s)));
        model.addAttribute("purged_variance_3", format(statisticService.getVariance(purged_series_3s)));
        model.addAttribute("purged_sigma_3", format(statisticService.getSigma(purged_series_3s)));

        return "statsanalyse";
    }

    /**
     * Print a double rounded at 2 decimals
     * @param i double
     * @return formatted string
     */
    public String format(double i)
    {
        return decimalFormat.format(i);
    }
}
