package projet_web_univers7.controllers;


import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import projet_web_univers7.domain.Car;
import projet_web_univers7.domain.CarModel;
import projet_web_univers7.domain.Course;
import projet_web_univers7.domain.DistanceTime;
import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;
import projet_web_univers7.domain.Reservation;
import projet_web_univers7.services.CarModelService;
import projet_web_univers7.services.CarService;
import projet_web_univers7.services.CourseService;

@Controller
public class CourseController {
	
	private DistanceTime distance;
	@Autowired
	private CourseService courseService;
	
	@Autowired
    public void setMessageService(CourseService courseService) {
        this.courseService = courseService;
    }
	@Autowired
    public void setDistance(DistanceTime distance) {
		this.distance = distance;
	}
	
	@GetMapping(value="/geoloc")
	public String loc() throws IOException {
		List<Course> courseList = courseService.getAllCourse();
		for(Course c : courseList) {
			
			Reservation resa = c.getId_resa();
			Place p = resa.getEndPlace();
			Parking park = p.getParking();
			String lat = park.getLatitude();
			String longitude = park.getLongitude();
			System.out.println("Course : " + c.getId() +"Lat : " + lat + " Longitude : "+ longitude);
			String response=distance.calculate(c.getLatitude(),c.getLongitude(),lat,longitude);
			JSONParser parser = new JSONParser();
	        try {

	         Object obj = parser.parse(response);
	         JSONObject jsonobj=(JSONObject)obj;

	         JSONArray dist=(JSONArray)jsonobj.get("rows");
	         JSONObject obj2 = (JSONObject)dist.get(0);
	         JSONArray disting=(JSONArray)obj2.get("elements");
	         JSONObject obj3 = (JSONObject)disting.get(0);
	         JSONObject obj5=(JSONObject)obj3.get("duration");
	         System.out.println(response);
	         long obj6 = (long) obj5.get("value");
	         
	         Timestamp now = new Timestamp(System.currentTimeMillis());
	         Timestamp end = c.getRealEndTimestamp();
	         long diff = end.getTime() - now.getTime();
	         long diffMinutes = diff / (60*1000) % 60;
	         long retard= diffMinutes - (obj6/60);
	   
	         System.out.println(diffMinutes);
	         System.out.println(retard);
	        }
	        catch(Exception e) {
	        		e.printStackTrace();
	        }
		}
		return "geoloc";
	}
	
	//Add
	@GetMapping(value = "/courses")
	public String greetingForm(final Model model) throws IOException {
	  
	   List<Course> courseList = courseService.getAllCourse();
	   List<Course> lateCourse =null;
		for(Course c : courseList) {
			
			Reservation resa = c.getId_resa();
			Place p = resa.getEndPlace();
			Parking park = p.getParking();
			String lat = park.getLatitude();
			String longitude = park.getLongitude();
			System.out.println("Course : " + c.getId() +"Lat : " + lat + " Longitude : "+ longitude);
			String response=distance.calculate(c.getLatitude(),c.getLongitude(),lat,longitude);
			JSONParser parser = new JSONParser();
	        try {

	         Object obj = parser.parse(response);
	         JSONObject jsonobj=(JSONObject)obj;

	         JSONArray dist=(JSONArray)jsonobj.get("rows");
	         JSONObject obj2 = (JSONObject)dist.get(0);
	         JSONArray disting=(JSONArray)obj2.get("elements");
	         JSONObject obj3 = (JSONObject)disting.get(0);
	         JSONObject obj5=(JSONObject)obj3.get("duration");
	         System.out.println(response);
	         long obj6 = (long) obj5.get("value");
	         
	         Timestamp now = new Timestamp(System.currentTimeMillis());
	         Timestamp end = c.getId_resa().getPrevisionEndTimestamp();
	         
	         long diff = end.getTime() - now.getTime();
	         long diffMinutes = diff / (60*1000) % 60;
	         long retard= diffMinutes - (obj6/60);
	         if(retard<0) {
	        	 	c.setLate(true);
	         }
	         else { c.setLate(false);}
	         Timestamp real =  Timestamp.from(now.toInstant().plusSeconds(obj6));
	         c.setRealEndTimestamp(real);
	         courseService.updateRealEndTimestamp(real, c.getId());
	         System.out.println(retard);
	        }
	        catch(Exception e) {
	        		e.printStackTrace();
	        }
		}
		List<Course> sortCourse = new ArrayList<Course>();
		for(Course c : courseList) {
			if(c.getLate() == true) {
				sortCourse.add(c);
			}
		}
		for(Course c : courseList) {
			if(c.getLate() == false) {
				sortCourse.add(c);
			}
		}
	   model.addAttribute("courses",sortCourse);
		
		
       return "courses";
    }
	
	
}
