package projet_web_univers7.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import projet_web_univers7.domain.Account;

import java.util.regex.Pattern;

@Component
public class RegistrationValidator extends AccountValidator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Account.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        super.validate(o,errors);

        // Checks
        this.validatePassword();
    }

    @Override
    public void validateNotEmpty()
    {
        super.validateNotEmpty();
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty", "Password can not be empty");
    }


    @Override
    public void validateUsername()
    {
        // Checking for username validity
        if (account.getUsername().length() < 6 || account.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username", "Password must be between 6 and 32 characters.");
        }
        if (accountService.findByUsername(account.getUsername()) != null) {
            errors.rejectValue("username", "Duplicate.userForm.username", "The login entered is already used.");
        }
    }

    public void validatePassword()
    {
        // Checking for passwords validity
        if (account.getPassword().length() < 6 || account.getPassword().length() > 32) {
            errors.rejectValue("password", "Size.userForm.password", "Password must have between 6 and 32 characters");
        }
        if (!account.getPasswordConfirm().equals(account.getPassword())) {
            errors.rejectValue("passwordConfirm", "Diff.userForm.passwordConfirm", "Password don't match");
        }
    }
}
