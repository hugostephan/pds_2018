package projet_web_univers7.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import projet_web_univers7.domain.Place;

@Component
public class PlaceValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Place.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Place place = (Place) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        if (place.getName().length() > 3 || place.getName().length()==0) {
            errors.rejectValue("name", "Size.place.name", "Name must have between 0 and 3 characters.");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "length", "NotEmpty");
        if (place.getLength().length() < 1) {
            errors.rejectValue("length", "Size.place.length", "Length must have characters.");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "width", "NotEmpty");
        if (place.getWidth().length() < 1) {
            errors.rejectValue("width", "Size.place.width", "Width must have characters.");
        }
    }
}