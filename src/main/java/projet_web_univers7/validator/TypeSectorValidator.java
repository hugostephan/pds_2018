package projet_web_univers7.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.thymeleaf.util.NumberUtils;
import projet_web_univers7.domain.TypeSector;
import projet_web_univers7.services.TypeSectorService;

@Component
public class TypeSectorValidator implements Validator{

    @Autowired
    TypeSectorService typeSectorService;

    @Override
    public boolean supports(Class<?> aClass) {
        return TypeSector.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        TypeSector typeSector = (TypeSector) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "percent", "NotEmpty");
        if (typeSector.getPercent() < 0 || typeSector.getPercent() > 0.5) {
            errors.rejectValue("percent", "Negative.typeSector.percent", "percent cannot be negative or superior 0.5");
        }
    }
}
