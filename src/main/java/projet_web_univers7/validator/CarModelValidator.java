package projet_web_univers7.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import projet_web_univers7.domain.CarModel;
import projet_web_univers7.services.CarModelService;

@Component
public class CarModelValidator implements Validator {

    @Autowired
    CarModelService carModelService;

    @Override
    public boolean supports(Class<?> aClass) {
        return CarModel.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        CarModel carModel = (CarModel) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hour_price", "NotEmpty");
        if (carModel.getHour_price().doubleValue() < 0 || carModel.getHour_price().doubleValue() > 999.99) {
            errors.rejectValue("hour_price", "Negative.carModel.hour_price", "hour price cannot be negative or superior 999.99");
        }
    }
}
