package projet_web_univers7.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import projet_web_univers7.domain.Account;
import projet_web_univers7.services.AccountService;

import java.util.regex.Pattern;


/**
 * User validator to valid or reject User registration data
 * @author Hugo
 */
@Component
public class AccountValidator implements Validator {
    // Errors
    protected Errors errors ;
    protected Account account ;
    protected Pattern expr ;

    @Autowired
    protected AccountService accountService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Account.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        // Fetching entered data
        this.account = (Account) o;
        this.errors = errors ;

        // Checks
        this.validateNotEmpty();
        this.validateUsername();
        this.validateAlphanumFields();
        this.validateDate();
        this.validatePhoneNumber();
        this.validateEmail();
    }

    public void validateNotEmpty()
    {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty", "Username can not be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstname", "Firstname.empty", "Firstname can't be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "Lastname.empty", "Lastname can't be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthDate", "Birthdate.empty", "Birthdate can't be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phoneNumber", "PhoneNumber.empty", "Phone Number can't be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "Email.empty", "E-mail can't be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "City.empty", "City can't be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "Address.empty", "Address can't be empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "postalCode", "PostalCode.empty", "Postal Code can't be empty");
    }

    public void validateUsername()
    {
        // Checking for username validity
        if (account.getUsername().length() < 6 || account.getUsername().length() > 32) {
            errors.rejectValue("username", "Size.userForm.username", "Password must be between 6 and 32 characters.");
        }
    }

    public void validateAlphanumFields()
    {
        expr = Pattern.compile("[a-zA-Z]|[0-9]");
        if(!expr.matcher(account.getFirstname()).find())
        {
            errors.rejectValue("firstname", "Firstname.badValue", "Firstname can only contains alphanumeric elements.");
        }
        if(!expr.matcher(account.getLastname()).find())
        {
            errors.rejectValue("lastname", "Lastname.badValue", "Lastname can only contains alphanumeric elements.");
        }
        if(!expr.matcher(account.getCity()).find())
        {
            errors.rejectValue("city", "City.badValue", "City can only contains alphanumeric elements.");
        }
        if(!expr.matcher(account.getAddress()).find())
        {
            errors.rejectValue("address", "Address.badValue", "Address can only contains alphanumeric elements.");
        }
        if(!expr.matcher(account.getPostalCode()).find())
        {
            errors.rejectValue("postalCode", "PostalCode.badValue", "Postal Code can only contains alphanumeric elements.");
        }
    }

    public void validateDate()
    {
        expr = Pattern.compile("[\\d]{2}/[\\d]{2}/[\\d]{2,4}");
        if(!expr.matcher(account.getBirthDate()).find())
        {
            errors.rejectValue("birthDate", "BirthDate.badValue", "Birth Date must match DD/MM/YYYY.");
        }
    }

    public void validatePhoneNumber()
    {
        expr = Pattern.compile("[0-9]{8,13}");
        if(!expr.matcher(account.getPhoneNumber()).find())
        {
            errors.rejectValue("phoneNumber", "PhoneNumber.badValue", "Phone Number can only contains numbers.");
        }
    }

    public void validateEmail()
    {
        expr = Pattern.compile("^[\\w\\-.+_%]+@[\\w\\.\\-]+\\.[A-Za-z0-9]{2,}$");
        if(!expr.matcher(account.getEmail()).find())
        {
            errors.rejectValue("email", "Email.badValue", "Email must match xxxxx@yyyy.zzz.");
        }
    }
}