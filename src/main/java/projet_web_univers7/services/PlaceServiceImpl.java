package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;
import projet_web_univers7.repositories.ParkingRepository;
import projet_web_univers7.repositories.PlaceRepository;

import java.util.List;

@Service
public class PlaceServiceImpl implements PlaceService {
    @Autowired
    private PlaceRepository placeRepo;

    @Autowired
    private ParkingRepository parkRepo;


    @Override
    public void addPlace(Place place,Integer id) {
        Parking park = parkRepo.findById(new Long(id));
        place.setParking(park);
        placeRepo.save(place);
    }

    public Place getPlace(String name) {
    		return placeRepo.getByName(name);
    }
    @Override
    public List<Place> listPlaceByParking(Parking parking){

        Parking park = parkRepo.findById(new Long(parking.getId()));
        //List<Place> list = placeRepo.findAllByParking(park);
        return placeRepo.findAllByParking(park);
    }

    @Override
    public List<Place> listAll(){
        return (List<Place>) placeRepo.findAll();
    }
}
