package projet_web_univers7.services;

import projet_web_univers7.domain.Account;

/**
 * Security service interface to manage Security context and authenticated users from db
 * @author Hugo
 */
public interface SecurityService {
    void autologin(String username, String password);
    boolean isAuthenticated();
    boolean isAdministrator();
    Account getAuthenticatedUser();
}