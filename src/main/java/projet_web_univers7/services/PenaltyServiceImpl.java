package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Penalty;
import projet_web_univers7.repositories.PenaltyRepository;

import java.util.List;

@Service
public class PenaltyServiceImpl implements PenaltyService{

    @Autowired
    private PenaltyRepository penaltyRepository;

    @Override
    public Penalty savePenalty(Penalty penalty) {
        return penaltyRepository.save(penalty);
    }

    @Override
    public Penalty findById(int id) {
        return penaltyRepository.findOne(id);
    }

    @Override
    public List<Penalty> findAll() {
        return penaltyRepository.findAll();
    }
}
