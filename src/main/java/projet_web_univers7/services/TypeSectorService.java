package projet_web_univers7.services;

import java.util.List;
import projet_web_univers7.domain.TypeSector;

public interface TypeSectorService {

    List<TypeSector> getAllTypeSector();

    TypeSector saveTypeSector(TypeSector typeSector);

    void deleteTypeSector(TypeSector typeSector);

    TypeSector getTypeSectorById(int id);
}
