package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Historic;
import projet_web_univers7.repositories.HistoricRepository;

import java.sql.Timestamp;
import java.util.List;

@Service
public class HistoricServiceImpl implements HistoricService {

    @Autowired
    private HistoricRepository historicRepository;

    @Override
    public Historic saveHistoric(Historic historic) {
        return historicRepository.save(historic);
    }

    @Override
    public Historic findById(int id) {
        return historicRepository.findOne(id);
    }

    @Override
    public List<Historic> findAll() {
        return historicRepository.findAll();
    }

    @Override
    public List<Historic> findHistoricsByAccountAndDate(int idAccount, Timestamp startDate, Timestamp endDate){
        List<Historic> lHistoric = historicRepository.findAllByAccount_IdAndRealEndTimestampBetween(idAccount, startDate, endDate);
        return lHistoric;
    }
}
