package projet_web_univers7.services;

import projet_web_univers7.domain.Historic;
import projet_web_univers7.domain.MonthStat;

import java.text.ParseException;
import java.util.ArrayList;

public interface DelayService {
    int getNumberDelay();
    double getRateDelay();
    int getNumberHistoric();
    int getNumberHistoricByMonth(String month);
    int getNumberDelayByMonth(String month);
    double getRateDelayByMonth(String month);
    ArrayList<Historic> getDelayHistoric();
    public double [] getDelayedSeries();
    public ArrayList<MonthStat> getStatByMonth();
}
