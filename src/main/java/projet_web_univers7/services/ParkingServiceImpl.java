package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;
import projet_web_univers7.repositories.ParkingRepository;

import java.util.List;

@Service
public class ParkingServiceImpl implements ParkingService{
    @Autowired
    private ParkingRepository parkRepo;


    @Override
    public List<Parking> listAllParking() {
        return parkRepo.findAll();
    }

    @Override
    public Parking getParkingById(int id) {
        return parkRepo.findById(new Long(id));
    }

    @Override
    public void saveParking(Parking parking) {
        parkRepo.save(parking);
    }
}
