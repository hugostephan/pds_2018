package projet_web_univers7.services;

import projet_web_univers7.domain.Message;

import java.util.List;

public interface MessageService {
    List<Message> listAllMessages();

    Message getMessageById(Integer id);

    Message saveMessage(Message message);

    void deleteMessage(Integer id);
}
