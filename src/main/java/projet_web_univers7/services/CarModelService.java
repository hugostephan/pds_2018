package projet_web_univers7.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import projet_web_univers7.domain.CarModel;
import projet_web_univers7.repositories.CarModelRepo;
import projet_web_univers7.repositories.CarRepo;

@Service
@Transactional
public class CarModelService {

    @Autowired
    private CarModelRepo cmRepo;

    public CarModelService() {
    }

    @Autowired
    public void setCarModelService(CarModelRepo cmRepo) {
        this.cmRepo = cmRepo;
    }


    public CarModel insertModel(CarModel cm) {
        return cmRepo.save(cm);
    }

    public List<String> getAllModels() {
        return (List<String>) cmRepo.getModelName();
    }

    public List<String> getDistinctBrand() {
        List<String> al = new ArrayList<String>();
        al = cmRepo.getByBrand();
        return al;
    }
    
    public List<String> getDistinctModel() {
        List<String> al = new ArrayList<String>();
        al = cmRepo.getByModel();
        return al;
    }
    
    public List<String> getDistinctType() {
        List<String> al = new ArrayList<String>();
        al = cmRepo.getByType();
        return al;
    }
    
    public List<String> getDistinctCapacity() {
        List<String> al = new ArrayList<String>();
        al = cmRepo.getByCapacity();
        return al;
    }
    
    public List<String> getDistinctGs() {
        List<String> al = new ArrayList<String>();
        al = cmRepo.getByGs();
        return al;
    }

    public CarModel getCarModelById(int id) {
        return cmRepo.findOne(id);
    }

    public CarModel findCarModelByBrand(String brand, String model) {
        return cmRepo.getCarModelByBrandModel(brand, model);
    }

    public List<CarModel> findAllModel() {
        return (List<CarModel>) cmRepo.findAll();
    }
}
