package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Historic;
import projet_web_univers7.domain.MonthStat;
import projet_web_univers7.domain.Reservation;
import projet_web_univers7.repositories.HistoricRepository;
import projet_web_univers7.repositories.ReservationRepository;

import java.sql.Timestamp;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service
public class DelayServiceImpl implements DelayService {
    // Format variables
    private static DecimalFormat decimalFormat = new DecimalFormat(".##");
    private static String stringFormat = new String("%02d");
    private static DateFormatSymbols dfs = new DateFormatSymbols(Locale.FRENCH);

    @Autowired
    private HistoricRepository repository;

    @Override
    public int getNumberDelay(){
        List<Historic> listHistoric = repository.findAll();
        int nbDelay=0;
        for (Historic historic : listHistoric){
            if (historic.getRealEndTimestamp().compareTo(historic.getPrevisionEndTimestamp())>0){
                nbDelay++;
            }
        }
        return nbDelay;
    }

    @Override
    public double getRateDelay() {
        int nbHisto = repository.getNumberHistoric();
        int nbDelay= this.getNumberDelay();
        double rateDelay=0.0;
        if (nbDelay !=0 && nbHisto!=0){
            rateDelay = (double) nbDelay/nbHisto;
        }

        return rateDelay;
    }

    @Override
    public int getNumberHistoric() {
        return repository.getNumberHistoric();
    }

    @Override
    public int getNumberHistoricByMonth(String month) {
        List<Historic> listHistoric = repository.findAll();

        int nbHisto=0;
        for (Historic historic : listHistoric){
            if (historic.getRealEndTimestamp().toString().contains("-"+month+"-")) {
                nbHisto++;
            }
        }
        return nbHisto;
    }

    @Override
    public double getRateDelayByMonth(String month) {
        int nbHisto = this.getNumberHistoricByMonth(month);
        int nbDelay= this.getNumberDelayByMonth(month);
        double rateDelay=0.0;
        if (nbDelay !=0 && nbHisto!=0){
            rateDelay = (double) nbDelay/nbHisto;
        }

        return rateDelay;
    }

    @Override
    public int getNumberDelayByMonth(String month) {
        List<Historic> listHistoric = repository.findAll();
        int nbDelay=0;
        for (Historic historic : listHistoric){
            if (historic.getRealEndTimestamp().toString().contains("-"+month+"-")) {
                if (historic.getRealEndTimestamp().compareTo(historic.getPrevisionEndTimestamp()) > 0) {
                    nbDelay++;
                }
            }
        }
        return nbDelay;
    }

    @Override
    public ArrayList<Historic> getDelayHistoric() {
        List<Historic> list = repository.findAll();
        ArrayList<Historic> listDelayed = new ArrayList<>();

        for(Historic h : list)
        {
            if (h.getRealEndTimestamp().compareTo(h.getPrevisionEndTimestamp()) > 0) {
                listDelayed.add(h);
            }
        }

        return listDelayed;
    }

    @Override
    public double [] getDelayedSeries() {
        ArrayList<Historic> listDelayed = this.getDelayHistoric();
        double [] series = new double[listDelayed.size()];
        int i = 0 ;
        Date d1,d2;
        for(Historic h : listDelayed)
        {
            d1 = new Date(h.getRealEndTimestamp().getTime());
            d2 = new Date(h.getPrevisionEndTimestamp().getTime());
            series[i] = ( (d1.getTime() - d2.getTime()) / (60*1000) );
            i++;
        }
        return series ;
    }


    @Override
    public ArrayList<MonthStat> getStatByMonth()
    {
        int i = 0;
        ArrayList<MonthStat> listStat = new ArrayList<MonthStat>();
        String [] listMonth = dfs.getMonths();

        for (i=1;i<13;i++)
        {
            MonthStat mt = new MonthStat();
            mt.setMonth(listMonth[i-1].toUpperCase());
            mt.setNb_delay(this.getNumberDelayByMonth(format(i)));
            mt.setNb_histo(this.getNumberHistoricByMonth(format(i)));
            mt.setRate(format(this.getRateDelayByMonth(format(i))*100)+" %");
            listStat.add(mt);
        }

        return listStat;
    }

    /**
     * Print an integer on 2 digits (01, 02, 03...)
     * @param i integer
     * @return formatted string
     */
    public String format(int i)
    {
        return String.format(stringFormat,i);
    }

    /**
     * Print a double rounded at 2 decimals
     * @param i double
     * @return formatted string
     */
    public String format(double i)
    {
        return decimalFormat.format(i);
    }
}
