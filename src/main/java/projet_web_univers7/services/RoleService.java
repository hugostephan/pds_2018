package projet_web_univers7.services;

import projet_web_univers7.domain.Role;

import java.util.List;

public interface RoleService {
    List<Role> listAllRole();

    Role getRoleById(Integer id);

    Role getRoleByName(String name);

    Role saveRole(Role role);

    void deleteRole(Integer id);
}
