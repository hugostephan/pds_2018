package projet_web_univers7.services;
import java.util.Random;

/**
 * Random gaussian number generator
 * @author Hugo STEPHAN
 */
public final class RandomGaussian
{
    private Random fRandom = new Random();
    private double mean = 15 ;
    private double variance = 5 ;
    private int exotic_number ;
    private int exotic_maximum = 100;
    private int exotic_minimum = 1;

    /**
     * Init generator with a mean and a variance
     * @param m mean
     * @param v variance
     */
    public RandomGaussian(double m, double v, int freq, int min, int max)
    {
        this.mean = m ;
        this.variance = v ;
        this.exotic_minimum = min ;
        this.exotic_maximum = max ;

        if(freq > 0) {
            this.exotic_number = freq ;
        }
        else
        {
            this.exotic_number = -1 ;
        }
    }

    /**
     * Return a random gaussian number
     * @return
     */
    public double getGaussianNumber()
    {
        return Math.abs ( fRandom.nextGaussian() * variance + mean);
    }

    /**
     * Return an array of random gaussian number
     * @param n array's size
     * @return array of random gaussian number
     */
    public double[] getNGaussianNumber(int n)
    {
        double tab[] = new double[n];
        int i = 0 ;
        for(i=0;i<n;i++)
        {
            tab[i] = this.getGaussianNumber();
        }
        return tab;
    }

    /**
     * 'Exotize' a gaussian series by adding exotic number in it (random 1 to max number) according to the given exotic frequency
     * @param list list to exotize
     * @return exotized series
     */
    public double[] exotize(double [] list)
    {
        double [] ret = list ;
        int modulo = ret.length/exotic_number;

        int i = 0 ;
        for(double d : ret)
        {
            if((i+1) % modulo == 0)
            {
                ret[i] = this.getExoticNumber(exotic_minimum, exotic_maximum);
            }

            i++;
        }

        return ret ;
    }

    /**
     * Generate an exotic random number (exotic because not gaussian) between the given min and max values.
     * @param min Minimal value for the exotic random number
     * @param max Maximal value for the exotic random number
     * @return exotic number
     */
    public double getExoticNumber(double min, double max) {
        Random r = new Random();
        double ret = r.nextDouble() * (max - min) + min;
        return ret;
    }
}