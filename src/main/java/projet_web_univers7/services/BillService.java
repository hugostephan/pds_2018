package projet_web_univers7.services;

import projet_web_univers7.domain.Bill;

import java.util.List;

public interface BillService {

    Bill saveBill(Bill bill);
    Bill findById(int id);

    List<Bill> findAll();
}
