package projet_web_univers7.services;

import projet_web_univers7.domain.Penalty;

import java.util.List;

public interface PenaltyService {

    Penalty savePenalty(Penalty penalty);
    Penalty findById(int id);

    List<Penalty> findAll();
}
