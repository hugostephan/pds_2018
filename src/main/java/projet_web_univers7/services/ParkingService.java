package projet_web_univers7.services;

import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Parking;

import java.util.List;


public interface ParkingService {
    List<Parking> listAllParking();
    Parking getParkingById(int id);
    void saveParking(Parking parking);
}
