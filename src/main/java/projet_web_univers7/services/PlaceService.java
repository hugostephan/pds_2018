package projet_web_univers7.services;

import projet_web_univers7.domain.Parking;
import projet_web_univers7.domain.Place;

import java.util.List;

public interface PlaceService {
    void addPlace(Place place, Integer id);
    List<Place> listPlaceByParking(Parking parking);
    List<Place> listAll();
    public Place getPlace(String name);
}
