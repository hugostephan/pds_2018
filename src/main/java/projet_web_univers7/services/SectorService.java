package projet_web_univers7.services;

import projet_web_univers7.domain.Sector;

import java.util.List;

public interface SectorService {

    List<Sector> getAllSector();

    Sector saveSector(Sector sector);

    void deleteSector(Sector sector);

    Sector getSectorById(int id);
}
