package projet_web_univers7.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import projet_web_univers7.domain.Account;
import projet_web_univers7.repositories.AccountRepository;
import projet_web_univers7.repositories.RoleRepository;
import projet_web_univers7.validator.AccountValidator;
import projet_web_univers7.validator.RegistrationValidator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;


@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private AccountValidator accountValidator;
    @Autowired
    private RegistrationValidator registrationValidator;

    @Override
    public BindingResult validateCreateAccount(Account acc, BindingResult br)
    {
        registrationValidator.validate(acc, br);
        return br ;
    }

    @Override
    public BindingResult validateUpdateAccount(Account acc, BindingResult br)
    {
        accountValidator.validate(acc,br);
        return br ;
    }

    @Override
    public Account saveAccount(Account acc)
    {
        if(acc.getUsername()==null)
        {
            return null ;
        }
        if(this.findByUsername(acc.getUsername())!=null)
        {
            acc.setPassword(this.findByUsername(acc.getUsername()).getPassword());
            acc.setPasswordConfirm(acc.getPassword());
        }
        else if((acc.getPassword() != null) && (acc.getPasswordConfirm() != null)) {
            acc.setPassword(bCryptPasswordEncoder.encode(acc.getPassword()));
            acc.setPasswordConfirm(bCryptPasswordEncoder.encode(acc.getPasswordConfirm()));
        }
        else
        {
            return null ;
        }

        if(accountRepository.findByUsername(acc.getUsername()) == null)
        {
            String pattern = "dd/MM/yyyy";
            String regDate =new SimpleDateFormat(pattern).format(new Date());

            acc.setRegistrationDate(regDate);
        }

        Account account = accountRepository.save(acc);
        return account;
    }

    @Override
    public Account findByUsername(String username) {
        return accountRepository.findByUsername(username);
    }

    @Override
    public Account findById(String id) {
        Pattern expr = Pattern.compile("[0-9]*");
        if(!expr.matcher(id).matches())
        {
            return null ;
        }
        return accountRepository.findById(Integer.parseInt(id));
    }

    @Override
    public List<Account> findAllByRole(String role) {
        List<Account> accounts = accountRepository.findAll();
        List<Account> ret = new ArrayList<Account>();

        for(Account a : accounts)
        {
            if(a.getRole().getName().equals(role))
            {
                ret.add(a);
            }
        }

        return ret;
    }

    @Override
    public List<Account> findCustomersWithFilters(HashMap<String, String> crit) {
        List<Account> accounts = accountRepository.findAll();
        List<Account> ret = new ArrayList<>();

        for(Account a : accounts)
        {
            if(a.getRole().getName().equals("customer")) {
                if (crit.containsKey("PRENOM") && a.getFirstname().contains(crit.get("PRENOM"))) {
                    ret.add(a);
                }
                if (crit.containsKey("NOM") && a.getLastname().contains(crit.get("NOM"))) {
                    ret.add(a);
                }
                if (crit.containsKey("TELEPHONE") && a.getPhoneNumber().contains(crit.get("TELEPHONE"))) {
                    ret.add(a);
                }
                if (crit.containsKey("VILLE") && a.getCity().contains(crit.get("VILLE"))) {
                    ret.add(a);
                }
            }
        }

        return ret;
    }

    @Override
    public List<Account> findCustomersByRegistritionDate(String date){
        List<Account> lAccountResult = accountRepository.findAccountsByRegistrationDateLike(date);
        return lAccountResult;
    }
}
