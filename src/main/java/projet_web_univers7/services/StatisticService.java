package projet_web_univers7.services;

public interface StatisticService {
    public double getMean(double[] series);
    public double getVariance(double [] series);
    public double getSigma(double [] series);

    // Abnormal Data
    public double[] getExceptionalData(double [] series);
    public double [] getDataBiggerThanNSigma(double [] series, int nb_sigma);
    public double [] getDataBiggerThan2Sigma(double [] series);
    public double [] getDataBiggerThan3Sigma(double [] series);
    public double [] purgeExeceptionalData(double [] series, int nb_sigma);
}
