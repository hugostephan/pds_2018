package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Bill;
import projet_web_univers7.repositories.BillRepository;

import java.util.List;

@Service
public class BillServiceImpl implements BillService {

    @Autowired
    private BillRepository billRepository;

    @Override
    public Bill saveBill(Bill bill) {
        return billRepository.save(bill);
    }

    @Override
    public Bill findById(int id) {
        return billRepository.findOne(id);
    }

    @Override
    public List<Bill> findAll() {
        return billRepository.findAll();
    }
}
