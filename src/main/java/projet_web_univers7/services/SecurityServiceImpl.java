package projet_web_univers7.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Account;
import projet_web_univers7.domain.Role;
import projet_web_univers7.services.SecurityService;

/**
 * Security service class implementation to manage Security object from db
 * @author Hugo
 */
@Service
public class SecurityServiceImpl implements SecurityService {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AccountService accountService;

    // Variables
    private Account account;
    private Role role;
    private boolean ret;


    @Override
    public void autologin(String username, String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());

        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            System.out.println("Auto login "+username+" successfully!");
        }
    }

    @Override
    public boolean isAdministrator()
    {
        ret = false ;
        account = this.getAuthenticatedUser();
        role = account.getRole();

        if(role.getName().equals("backoffice"))
        {
            ret = true ;
        }

        return ret ;
    }

    /**
     * Return authenticated user's account
     * @return Authenticated user Account or null
     */
    @Override
    public Account getAuthenticatedUser()
    {
        account = null ;

        if(this.isAuthenticated()) {
            account = accountService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        }

        return account ;
    }

    /**
     * Check user's authentication by trying to fetch user's account from db based on spring security context authentication name (= login)
     * @return
     */
    @Override
    public boolean isAuthenticated()
    {
        ret = false;
        account = null ;

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth != null) {
            account = accountService.findByUsername(auth.getName());
            if(account != null)
            {
                ret = true ;
            }
        }

        return ret;
    }
}