package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import projet_web_univers7.domain.Sector;
import projet_web_univers7.repositories.SectorRepository;

import java.util.List;

public class SectorServiceImpl implements SectorService {

    @Autowired
    private SectorRepository sectorRepository;

    @Override
    public List<Sector> getAllSector() {
        return sectorRepository.findAll();
    }

    @Override
    public Sector saveSector(Sector sector) {
        return sectorRepository.save(sector);
    }

    @Override
    public void deleteSector(Sector sector) {
        sectorRepository.delete(sector);
    }

    @Override
    public Sector getSectorById(int id) {
        return sectorRepository.findOne(id);
    }
}
