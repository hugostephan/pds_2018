package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.*;
import projet_web_univers7.repositories.*;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Data initialization class, Fill DB with arbitrary data
 * @author Hugo STEPHAN
 */
@Service
@Configurable
public class InitData {
    /**
     * Autowired services to fill DB
     */
    @Autowired
    AccountService accountService;

    @Autowired
    RoleService roleService;

    @Autowired
    ParkingService parkingService;

    @Autowired
    PlaceService placeService;

    @Autowired
    TypeSectorService typeSectorService;

    @Autowired
    CarModelService carModelService;
    
    @Autowired
    CarService carService;

    @Autowired
    CarRepo cRepo;
    
    @Autowired
    CourseService courseService;
    
    @Autowired
    ReservationService reservationService;

    @Autowired
    PlaceRepository pRepo;

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    HistoricRepository historicRepository;

    // Beans
    Account account ;
    Role r_bo_admin, r_customer ;
    Reservation resa ;
    Historic historic;
    RandomGaussian generator;

    /**
     * Generate test user and role
     */
    public void globalInit() throws ParseException {
        this.initRoles();
        this.initAdminAccount();
        this.initCustomersAccounts();
        this.initCarModel();
        this.initParking();
        this.initTypeSector();
        this.initHistoric();
        this.initCourse();
    }

    public void initRoles()
    {
        r_bo_admin = new Role();
        r_customer = new Role();

        r_bo_admin.setCode("BO");
        r_bo_admin.setDescription("Back Office Admin");
        r_bo_admin.setId(1);
        r_bo_admin.setName("backoffice");

        r_customer.setCode("CU");
        r_customer.setDescription("Customer");
        r_customer.setId(2);
        r_customer.setName("customer");

        roleService.saveRole(r_bo_admin);
        roleService.saveRole(r_customer);
    }

    /**
     *  Creating back office admin
     */
    public void initAdminAccount()
    {
        account = new Account();
        //account.setId(1);
        account.setAddress("Test Address");
        account.setBirthDate("08/05/1994");
        account.setCity("Creteil");
        account.setEmail("hugostephanbac@gmail.com");
        account.setFirstname("Hugo");
        account.setLastname("STEPHAN");
        account.setPassword("azertyuiop");
        account.setPasswordConfirm("azertyuiop");
        account.setPhoneNumber("0123456789");
        account.setPostalCode("94380");
        account.setUsername("administrateur");
        account.setRole(r_bo_admin);
        accountService.saveAccount(account);
    }

    /**
     *  Creating customers accounts
     */
    public void initCustomersAccounts()
    {
        int i;
        for(i=0;i<20;i++)
        {
            account = new Account();
            //account.setId(i+2);
            account.setAddress(i + " rue de la boucle");
            account.setBirthDate("01/01/1990");
            account.setCity("Ville " + i%4);
            account.setEmail("client_imaginaire" + i + "@mail.com");
            account.setFirstname("Client " + i);
            account.setLastname("NOM IMAGINAIRE " + (i+1)%4);
            account.setPassword("password"+i);
            account.setPasswordConfirm("password"+i);
            account.setPhoneNumber("0123456780");
            account.setPostalCode("94380");
            account.setUsername("customer_"+i);
            account.setRole(r_customer);
            accountService.saveAccount(account);
        }
    }

    public void initCarModel(){
        CarModel cm = new CarModel("Peugeot","207","Essence",new BigDecimal("17"),"Test","Citadine",5);
        Car c = new Car(cm, 2006, "BP228TA");
        CarModel cm2 = new CarModel("Peugeot","208","Essence",new BigDecimal("18"),"Test","Citadine",5);
        CarModel cm3 = new CarModel("Peugeot","308","Essence",new BigDecimal("19"),"Test","Citadine",5);
        CarModel cm4 = new CarModel("Renault","Clio","Essence",new BigDecimal("20"),"Test","Citadine",5);
        CarModel cm5 = new CarModel("Renault","Megane","Diesel",new BigDecimal("21"),"Test","Citadine",5);
        CarModel cm6 = new CarModel("Renault","Espace","Essence",new BigDecimal("22"),"Test","Espace",7);
        Car c2 = new Car(cm6, 2012,"CG545PO");
        CarModel cm7 = new CarModel("Audi","A3","Essence",new BigDecimal("23"),"Test","Sportive",5);
        CarModel cm8 = new CarModel("Audi","RS3","Essence",new BigDecimal("40"),"Test","Sportive",5);
        CarModel cm9 = new CarModel("Audi","RS6","Essence",new BigDecimal("60"),"Test","Sportive",5);
        Car c3 = new Car(cm9, 2018,"CJ509ME");


        carModelService.insertModel(cm);
        carModelService.insertModel(cm2);
        carModelService.insertModel(cm3);
        carModelService.insertModel(cm4);
        carModelService.insertModel(cm5);
        carModelService.insertModel(cm6);
        carModelService.insertModel(cm7);
        carModelService.insertModel(cm8);
        carModelService.insertModel(cm9);
        carService.insertCar(c);
        carService.insertCar(c2);
        carService.insertCar(c3);
    }

    public void initTypeSector(){
        TypeSector ts1 = new TypeSector("white", 0);
        TypeSector ts2 = new TypeSector("orange", 0.05);
        TypeSector ts3 = new TypeSector("red", 0.10);
        TypeSector ts4 = new TypeSector("black", 0.15);

        typeSectorService.saveTypeSector(ts1);
        typeSectorService.saveTypeSector(ts2);
        typeSectorService.saveTypeSector(ts3);
        typeSectorService.saveTypeSector(ts4);
    }

    public void initSector(){
        TypeSector typeSector = new TypeSector();
        typeSector.setColor("Black");
        typeSector.setPercent(20);
        Sector sector = new Sector();
        sector.setName("Concorde");
        sector.setPostalCode("75013");
        sector.setTown("Paris");
        sector.setTypeSector(typeSector);
    }

    public void initParking()
    {
/*
        TypeSector typeSector = new TypeSector();
        typeSector.setColor("Black");
        typeSector.setPercent(20);

        Sector sector = new Sector();
        sector.setName("Concorde");
        sector.setPostalCode("75013");
        sector.setTown("Paris");
        sector.setTypeSector(typeSector);
*/

        Parking parking = new Parking();
        parking.setCapacity(3);
        parking.setAddress("15 place de la Concorde");
        parking.setLatitude("48.865633");
        parking.setLongitude("2.321236");
        parking.setName("Place de la Concorde");
        //parking.setSector(sector);
        
        Place place = new Place();
        place.setName("A1");
        place.setLength("2");
        place.setWidth("1.75");
        List<Place> list = new ArrayList<Place>();
        list.add(place);

        
        parking.setPlaceCollection(list);

        parkingService.saveParking(parking);
        placeService.addPlace(place,1);
    }

    /**
     * Init historic data (ended courses)
     * @throws ParseException Error parsign date
     */
    public void initHistoric() throws ParseException {
        /**
         * Init RandomGaussian generator (mean, standard deviation, exotic frequency, exotic_minimal_value, exotic_maximum_value
         */
        this.generator = new RandomGaussian(17.24, 4.21, 241, 20, 60);

        // Get a list of gaussian "exotized" random numbers to be assigned as delay time, list size must fit delay's rate
        double [] list = generator.getNGaussianNumber(6667);
        list = generator.exotize(list);
        int cpt_late = 0 ;

        int i;
        for(i=0;i<100000;i++)
        {
            historic = new Historic();
            historic.setAccount(accountService.findByUsername("customer_" + ((i%21) + 1) ));
            historic.setCar(cRepo.findOne(i%30));
            historic.setEndPlace(pRepo.findOne(1));
            historic.setStartPlace(pRepo.findOne(1));
            if(i%17 == 0)
            {
                historic.setState("OVER_WITH_DELAY");
            }
            else
            {
                historic.setState("OVER_NORMAL");
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy / hh:mm");
            Date parsedDateEnd = dateFormat.parse(this.generateSequentialDatetime(i, 20));
            Timestamp timestampStart = new java.sql.Timestamp(parsedDateEnd.getTime());
            historic.setPrevisionEndTimestamp(timestampStart);
            if(i%17 == 0)
            {
                int late = 20 + (int) list[cpt_late];
                cpt_late++;

                Date parsedDate = dateFormat.parse(generateSequentialDatetime(i, late));
                Timestamp timestampEnd = new java.sql.Timestamp(parsedDate.getTime());

                historic.setRealEndTimestamp(timestampEnd);
            }
            else
            {
                Date parsedDate = dateFormat.parse(this.generateSequentialDatetime(i, 20));
                Timestamp timestampEnd = new java.sql.Timestamp(parsedDate.getTime());

                historic.setRealEndTimestamp(timestampEnd);
            }

            Date parsedDatePrevisionStart = dateFormat.parse(generateSequentialDatetime(i, 0));
            Timestamp timestampPrevisionStart = new java.sql.Timestamp(parsedDatePrevisionStart.getTime());
            historic.setPrevisionStartTimestamp(timestampPrevisionStart);

            Date parsedDateRealStart = dateFormat.parse(generateSequentialDatetime(i,0));
            Timestamp timestampRealStart = new java.sql.Timestamp(parsedDateRealStart.getTime());
            historic.setRealStartTimestamp(timestampRealStart);

            historicRepository.save(historic);
        }
    }

    /**
     * Generate a sequential DateTime String regarding the given number and minutes.
     * @param nb number (unique id)
     * @param minutes DateTime minutes
     * @return Sequential DateTime String
     */
    public String generateSequentialDatetime(int nb, int minutes)
    {
        // Make sure integer will be printed on two characters
        String f = "%02d" ;

        // Modulo magic : generates a sequential date accord to the given number and ending with the specified minutes
        return String.format(f,((nb)%31)+1) + "/" + String.format(f,(nb%12)+1) + "/2017 / " + String.format(f,nb%25) + ":" + String.format(f, minutes%60) ;
    }

    public void initCourse() {
        java.sql.Timestamp d1 = new Timestamp(System.currentTimeMillis());
        java.sql.Timestamp d2 = new Timestamp(System.currentTimeMillis()+3600*1000);
        java.sql.Timestamp d3 = new Timestamp(System.currentTimeMillis()-3600*1000);
        java.sql.Timestamp d4 = new Timestamp(System.currentTimeMillis()+60*1000);
        Place p = placeService.getPlace("A1");
        Account a = accountService.findById("2");
        Account b = accountService.findById("3");
        Account c = accountService.findById("4");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        Reservation r1 = new Reservation(d1,d2,p,a);
        Reservation r2 = new Reservation(d1,d2,p,b);
        Reservation r3 = new Reservation(d3,d4,p,c);

        reservationService.insertReservation(r1);
        reservationService.insertReservation(r2);
        reservationService.insertReservation(r3);

        Course c1 = new Course(r1,d1,d2,"2.45","48.9333");
        Course c2 = new Course(r2,d1,d2,"2.4667","48.7833");
        Course c3 = new Course(r3,d3,d4,"2.3599","48.9354");

        courseService.insertCourse(c1);
        courseService.insertCourse(c2);
        courseService.insertCourse(c3);
    }
}
