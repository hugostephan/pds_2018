package projet_web_univers7.services;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import projet_web_univers7.domain.Course;
import projet_web_univers7.domain.Reservation;
import projet_web_univers7.repositories.CourseRepository;
import projet_web_univers7.repositories.ReservationRepository;

@Service
@Transactional
public class CourseService {

	private CourseRepository cRepo;
	
	public CourseService() {}
	@Autowired
	public void setCourseService(CourseRepository cRepo) {
		this.cRepo = cRepo;
	}
	
	public List<Course> getAllCourse(){
		java.sql.Timestamp d1 = new Timestamp(System.currentTimeMillis());
		List<Course> l = (List<Course>) cRepo.findAllCourses(d1);
		System.out.println(l);
		return l;
	}
	
	
	public Course insertCourse(Course c) {
			return cRepo.save(c);
	}
	
	public void updateRealEndTimestamp(Timestamp real, int id) {
		cRepo.setRealEndTimestamp(real, id);
	}
	
}

