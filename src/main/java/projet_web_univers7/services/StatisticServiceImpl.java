package projet_web_univers7.services;

import org.springframework.stereotype.Service;
import projet_web_univers7.domain.MonthStat;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

@Service
public class StatisticServiceImpl implements StatisticService {
    private static DateFormatSymbols dfs = new DateFormatSymbols(Locale.FRENCH);

    @Override
    public double getMean(double [] series)
    {
        int n = series.length;
        double tot = 0 ;
        for(double d : series)
        {
            tot += d ;
        }

        return tot / n ;
    }

    @Override
    public double getVariance(double[] series)
    {
        int n = series.length;
        double mean = this.getMean(series);
        double tot = 0 ;
        for(double d : series)
        {
            tot += (d - mean)*(d - mean) ;
        }

        return tot / n ;
    }

    @Override
    public double getSigma(double [] series)
    {
        return Math.sqrt(this.getVariance(series));
    }

    @Override
    public double [] getExceptionalData(double [] series)
    {
        return this.getDataBiggerThan2Sigma(series);
    }

    @Override
    public double [] getDataBiggerThanNSigma(double [] series, int nb_sigma)
    {
        double mean = this.getMean(series);
        double sigma = this.getSigma(series);
        int nb = 0 ;
        int i = 0 ;

        for(double d : series)
        {
            if(d > (mean + nb_sigma * sigma))
            {
                nb++;
            }
        }

        double [] ret = new double[nb];

        for(double d : series)
        {
            if(d > (mean + nb_sigma * sigma))
            {
                ret[i] = d ;
                i++;
            }
        }

        return ret ;
    }

    @Override
    public double [] getDataBiggerThan2Sigma(double [] series)
    {
        return this.getDataBiggerThanNSigma(series, 2);
    }

    @Override
    public double [] getDataBiggerThan3Sigma(double [] series)
    {
        return this.getDataBiggerThanNSigma(series, 3);
    }

    @Override
    public double [] purgeExeceptionalData(double [] series, int nb_sigma)
    {
        double mean = this.getMean(series);
        double sigma = this.getSigma(series);
        int nb = 0 ;
        int i = 0 ;

        for(double d : series)
        {
            if(d < (mean + nb_sigma * sigma))
            {
                nb++;
            }
        }

        double [] ret = new double[nb];

        for(double d : series)
        {
            if(d < (mean + nb_sigma * sigma))
            {
                ret[i] = d ;
                i++;
            }
        }

        return ret ;
    }
}
