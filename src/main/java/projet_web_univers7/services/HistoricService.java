package projet_web_univers7.services;

import projet_web_univers7.domain.Historic;

import java.sql.Timestamp;
import java.util.List;

public interface HistoricService {

    Historic saveHistoric(Historic historic);
    Historic findById(int id);

    List<Historic> findAll();

    List<Historic> findHistoricsByAccountAndDate(int idAccount, Timestamp startDate, Timestamp endDate);
}
