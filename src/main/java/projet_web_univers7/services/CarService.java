package projet_web_univers7.services;

import java.util.List;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import projet_web_univers7.domain.Car;
import projet_web_univers7.domain.CarModel;

import projet_web_univers7.repositories.CarRepo;


@Service
@Transactional
public class CarService {

	private CarRepo vRepo;
	
	public CarService() {}
	@Autowired
	public void setUserService(CarRepo vRepo) {
		this.vRepo = vRepo;
	}
	
	public List<Car> getAllCars(){
		
		return (List<Car>) vRepo.findAll();
		
	}
	
	
	public List<String> getDistinctLn() {
        List<String> al = new ArrayList<String>();
        al = vRepo.getByLn();
        return al;
    }
	
	public List<String> getDistinctYear() {
        List<String> al = new ArrayList<String>();
        al = vRepo.getByYear();
        return al;
    }
	
	public Car insertCar(Car v) {
		List<Car> list = (List<Car>) vRepo.findAll();
		int i=0;
		for(Car c : list){
			if(c.getLicence_number().equals(v.getLicence_number())) {
				i++;
			}
		}
		if(i==0) {
			return vRepo.save(v);
		}
		else {
			return new Car();
		}
	}
	public Car createVehicle(Car v) {
		Car vehicle = v;
		return vehicle;
	}
}
