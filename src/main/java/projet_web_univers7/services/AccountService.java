package projet_web_univers7.services;

import org.springframework.validation.BindingResult;
import projet_web_univers7.domain.Account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface AccountService {
    Account saveAccount(Account acc);
    BindingResult validateCreateAccount(Account acc, BindingResult br);
    BindingResult validateUpdateAccount(Account acc, BindingResult br);
    Account findByUsername(String username);
    Account findById(String id);

    List<Account> findAllByRole(String role);
    List<Account> findCustomersWithFilters(HashMap<String,String> crit);
    List<Account> findCustomersByRegistritionDate(String date);
}
