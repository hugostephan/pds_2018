package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.TypeSector;
import projet_web_univers7.repositories.TypeSectorRepository;

import java.util.List;

@Service
public class TypeSectorServiceImpl implements TypeSectorService {

    @Autowired
    private TypeSectorRepository typeSectorRepository;

    @Override
    public List<TypeSector> getAllTypeSector() {
        return typeSectorRepository.findAll();
    }

    @Override
    public TypeSector saveTypeSector(TypeSector typeSector) {
        return typeSectorRepository.save(typeSector);
    }

    @Override
    public void deleteTypeSector(TypeSector typeSector) {
        typeSectorRepository.delete(typeSector);
    }

    @Override
    public TypeSector getTypeSectorById(int id) {
        return typeSectorRepository.findOne(id);
    }
}
