package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Role;
import projet_web_univers7.repositories.RoleRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService
{
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<Role> listAllRole() {
        List<Role> listRole = roleRepository.findAll();
        return listRole;
    }

    @Override
    public Role getRoleById(Integer id) {
        Role role = roleRepository.findOne(id);
        return role;
    }

    @Override
    public Role getRoleByName(String name) {
        Role ret = null ;
        List<Role> roles = roleRepository.findAll();
        for(Role r : roles)
        {
            if(r.getName().equals(name))
            {
                ret = r ;
            }
        }

        return ret ;
    }

    @Override
    public Role saveRole(Role r) {
        Role role = roleRepository.save(r);
        return role;
    }

    @Override
    public void deleteRole(Integer id) {
        roleRepository.delete(id);
    }
}
