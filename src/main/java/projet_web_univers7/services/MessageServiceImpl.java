package projet_web_univers7.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import projet_web_univers7.domain.Message;
import projet_web_univers7.repositories.MessageRepository;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService{

    @Autowired
    private MessageRepository msgRepo;

    @Override
    public List<Message> listAllMessages() {
        List<Message> lMsg = msgRepo.findAll();
        return lMsg;
    }

    @Override
    public Message getMessageById(Integer id) {
        Message msg = msgRepo.findOne(id);
        return msg;
    }

    @Override
    public Message saveMessage(Message message) {
        Message msg = msgRepo.save(message);
        return msg;
    }

    @Override
    public void deleteMessage(Integer id) {
        msgRepo.delete(id);
    }
}
