package projet_web_univers7.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import projet_web_univers7.domain.Car;
import projet_web_univers7.domain.Reservation;
import projet_web_univers7.repositories.CarRepo;
import projet_web_univers7.repositories.ReservationRepository;

@Service
@Transactional
public class ReservationService {

	private ReservationRepository rRepo;
	
	public ReservationService() {}
	@Autowired
	public void setReservationService(ReservationRepository rRepo) {
		this.rRepo = rRepo;
	}
	
	public List<Reservation> getAllReservation(){
		return (List<Reservation>) rRepo.findAll();
	}
	
	public Reservation insertReservation(Reservation r) {
			return rRepo.save(r);
	}
	
}
