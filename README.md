Repository PROJET_WEB_UNIVERS7 :
-
**Projet portail web CSC du groupe Univers7 pour le PDS de l'ESIPE**

Le dépôt contient les éléments suivants :

- `diagrams` : les différents diagrammes d'analyse et de conception pour nos différents UC.
- `src/main` : le code source de l'application en elle même.
- `src/main` : le code source des tests unitaires de l'application.
- `pom.xml` : le pom.xml de l'application qui contient la configuration Maven du projet, ajouté au dossier src, cela suffit à importer le projet sur un IDE moderne.
- `.gitignore` : permet d'exclure les éléments inutiles du dossier projet en local de git (fichiers de configuration des IDE et dossier target).
- `readme.md` : ce fichier descriptif du dépôt. 

Le dépôt comporte au minimum les branches suivantes :

- `master` : le "tronc" principal du dépôt, la seule branche vraiment représentative de l'application.
- `int` : la branche d'intégration qui nous sert à tester l'intégration de nos différentes parties de l'application.
- `dev` : la branche "lab" qui nous permet de faire de tests libres.
- et un nombre variable de feature branche (au moins une par scénario principal à priori).

Le dépôt est conçu comme un outil de travail, il permet de prendre facilement le projet en cours et d'y apporter sa contribution.


Pour commencer il faut se connecter au VPN de l'ESIPE/ESIAG/UPEC/SI/ING2/FISA, il est alors possible de cloner le dépôt pour travailler dessus avec la commande :

`git clone http://gitlab.univers7.inside.esiag.info/univers7/projet_web_univers7.git`

Si pour une raison quelconque le DNS est inacessible, on peut utiliser l'adresse IP :

`git clone http://192.168.20.6:8001/univers7/projet_web_univers7.git`

On peut alors importer le projet dans un IDE comme IntellIJ. 


On peut également lancer manuellement un certain nombre de commande notables depuis la racine du dossier :
- `git log` : Pour accéder à un historique git détaillé.
- `mvn javadoc:javadoc` : Pour générer la javadoc (ensuite accessible dans target/site/apidocs).
- `mvn package` : Pour générer un jar complétement exécutable (qui se place dans le sous dossier target).
- `java -jar target/projet_web_univers7-1.0-SNAPSHOT.jar` : Pour éxécuter le jar.


Le projet hebergé sur la branche master de ce dépôt est exécuté par un jenkins sur un serveur en permanence, et accessible à l'adresse :

http://www-int.univers7.inside.esiag.info

(Le projet est recompilé et donc les modifications sont actualisées toutes les 24heures)